package at.example.application;

import at.example.application.entity.Category;
import at.example.application.entity.News;
import at.example.application.repository.CategoryRepository;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * Class to handle category entities
 */
public class CategoryManager {
    CategoryRepository categoryRepository = MainApp.injector.getInstance(CategoryRepository.class);
    Map<String, Action> actions = new HashMap<>();

    /**
     * Construct a category manager instance
     */
    public CategoryManager() {
        actions.put("1", new ShowCategoriesAction());
        actions.put("2", new AddCategoriesFromCmdLineAction());
        actions.put("3", new AddCategoriesFromCsvAction());
        actions.put("4", new ChangeCategoryAction());
        actions.put("5", new DeleteAllCategoriesAction());
    }


    /**
     * Prints the menu for category actions
     */
    public void printMenu() {
        Scanner myObj = new Scanner(System.in);
        String actionKey;

        System.out.println("\n----------------------");
        System.out.println("Category Actions");
        for (String key : actions.keySet()) {
            System.out.println(key + "  " + actions.get(key));
        }
        actionKey = myObj.nextLine();
        if (actions.containsKey(actionKey)) {
            actions.get(actionKey).execute();
        }

    }

    /**
     * Saves the categories from a CSV into the DB
     */
    class AddCategoriesFromCsvAction implements Action {
        public void execute() {
            String pathToCsv = "categories.csv";
            try {
                BufferedReader csvReader = new BufferedReader(new FileReader(pathToCsv));
                String row;

                while ((row = csvReader.readLine()) != null) {

                    String[] data = row.split(",");
                    categoryRepository.save(new Category(data[0]));
                }
                csvReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public String toString() {
            return "add categories from CSV: \"categories.csv\" ";
        }
    }

    /**
     * Creates a category from the cmd line and saves into the DB
     */
    class AddCategoriesFromCmdLineAction implements Action {

        @Override
        public void execute() {
            Scanner myObj = new Scanner(System.in);
            System.out.println("Name: ");
            String name = myObj.nextLine();

            Category category = new Category(name);

            categoryRepository.save(category);
        }

        @Override
        public String toString() {
            return "Add categories from cmdline";
        }
    }

    /**
     * Prints all categories with their information
     */
    class ShowCategoriesAction implements Action {
        public void execute() {
            List<Category> categoryList = categoryRepository.findAll();

            System.out.println("Entries: " + categoryList.size());
            for (Category category : categoryList) {
                List<News> newsList = category.getNewsList();
                System.out.printf("\n--- %s with ID %s has %s articles!\n", category.getName(), category.getId(), newsList.size());
                for (var news : newsList) {
                    news.printNews();
                }
            }

        }

        @Override
        public String toString() {
            return "Show categories";
        }
    }

    /**
     * Changes an existing category
     */
    class ChangeCategoryAction implements Action {
        public void execute() {
            Scanner scanner = new Scanner(System.in);

            System.out.println("ID: ");
            String id = scanner.nextLine();
            scanner.reset();

            System.out.println("Name: ");
            String name = scanner.nextLine();

            Category category = new Category(Long.valueOf(id), name, null);

            categoryRepository.save(category);
        }

        @Override
        public String toString() {
            return "Change category";
        }
    }

    /**
     * Delete all categories from the DB
     */
    class DeleteAllCategoriesAction implements Action {
        public void execute() {
            categoryRepository.deleteAll();
        }

        @Override
        public String toString() {
            return "Delete all categories";
        }
    }


}
