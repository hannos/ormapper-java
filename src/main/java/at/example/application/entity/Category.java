package at.example.application.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

/**
 * The category entity
 */
@Data
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name = "t_category")
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 50)
    private String name;

    @ManyToMany
    private List<News> newsList;

    public Category(String name) {
        this(null, name, List.of());
    }

    public Category(String name, List<News> newsList) {
        this(null, name, newsList);
    }
}
