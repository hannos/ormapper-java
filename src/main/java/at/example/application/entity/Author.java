package at.example.application.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

/**
 * The author entity
 */
@Data
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name = "t_author")
public class Author {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 50)
    private String firstName;

    @Column(nullable = false, length = 50)
    private String lastName;

    @OneToMany(mappedBy = "author")
    private List<News> newsList;

    public Author(String firstName, String lastName, List<News> newsList) {
        this(null, firstName, lastName, newsList);
    }

    public Author(String firstName, String lastName) {
        this(null, firstName, lastName, List.of());
    }

    /**
     * Return the full name of the author
     *
     * @return full name of author
     */
    public String getName() {
        return firstName + " " + lastName;
    }
}
