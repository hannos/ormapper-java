package at.example.application.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

/**
 * The news entity
 */
@Data
@NoArgsConstructor
@AllArgsConstructor

@Entity
public class News {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 100, name = "a_title")
    private String title;

    @Column(nullable = false, length = 1000)
    private String text;

    @Column(nullable = false)
    private boolean topNews;

    @Column
    private Timestamp timestamp;

    @ManyToOne
    private Author author;

    @ManyToMany
    private List<Category> categories;

    public News(String title, String text, boolean topNews, Timestamp timestamp, Author author, List<Category> categories) {
        this(null, title, text, topNews, timestamp, author, categories);
    }

    public News(String title, String text, boolean topNews, Timestamp timestamp, Author author) {
        this(null, title, text, topNews, timestamp, author, List.of());
    }

    /**
     * Print the news
     */
    public void printNews() {
        System.out.printf("-- News with ID %s  Titel: %s --%n", id, title);
        System.out.println(text);
        if (author != null) {
            System.out.println("Author: " + author.getName());
        } else {
            System.out.println("Author: nicht angegeben\n");
        }

    }
}
