package at.example.application.repository;

import at.example.application.entity.News;
import at.oswald.jpql.JPQLQuery;
import at.oswald.repository.BaseJpaRepository;

import java.util.List;

/**
 * Data Repository for news entities
 */
public interface NewsRepository extends BaseJpaRepository<News, Long> {
    @JPQLQuery("select n from News n where n.title like ?1")
    List<News> findNewsWithTitleLike(String title);
}
