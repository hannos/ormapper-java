package at.example.application.repository;

import at.example.application.entity.Category;
import at.oswald.jpql.JPQLQuery;
import at.oswald.repository.BaseJpaRepository;

import java.util.List;

/**
 * Data Repository for category entities
 */
public interface CategoryRepository extends BaseJpaRepository<Category, Long> {
    @JPQLQuery("select c from Category c where c.name = ?1")
    List<Category> findCategoryByName(String name);
}
