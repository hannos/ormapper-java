package at.example.application.repository;

import at.example.application.entity.Author;
import at.oswald.jpql.JPQLQuery;
import at.oswald.repository.BaseJpaRepository;

import java.util.List;

/**
 * Data Repository for author entities
 */
public interface AuthorRepository extends BaseJpaRepository<Author, Long> {
    @JPQLQuery("select a from Author a where a.firstName = ?1")
    List<Author> findAuthorByFirstname(String firstName);

    @JPQLQuery("select a from Author a where a.firstName = ?1 and a.lastName = ?2")
    List<Author> findAuthorByFirstnameAndLastname(String firstName, String lastName);
}
