package at.example.application;

import at.example.application.entity.Author;
import at.example.application.entity.Category;
import at.example.application.entity.News;
import at.example.application.repository.AuthorRepository;
import at.example.application.repository.CategoryRepository;
import at.example.application.repository.NewsRepository;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;

/**
 * Class to handle news entities
 */
public class NewsManager {
    NewsRepository newsRepository = MainApp.injector.getInstance(NewsRepository.class);
    AuthorRepository authorRepository = MainApp.injector.getInstance(AuthorRepository.class);
    CategoryRepository categoryRepository = MainApp.injector.getInstance(CategoryRepository.class);
    Map<String, Action> actions = new HashMap<>();

    /**
     * Construct a news manager instance
     */
    public NewsManager() {
        actions.put("1", new ShowNewsAction());
        actions.put("2", new AddNewsFromCmdLineAction());
        actions.put("3", new AddNewsFromCsvAction());
        actions.put("4", new ChangeNewsAction());
        actions.put("5", new DeleteAllNewsAction());
    }


    /**
     * Prints the menu for news actions
     */
    public void printMenu() {
        Scanner scanner = new Scanner(System.in);
        String actionKey;

        System.out.println("\n----------------------");
        System.out.println("News Actions");
        for (String key : actions.keySet()) {
            System.out.println(key + "  " + actions.get(key));
        }
        actionKey = scanner.nextLine();
        if (actions.containsKey(actionKey)) {
            actions.get(actionKey).execute();
        }
    }

    /**
     * Build a news entity from strings
     *
     * @param title           title as string
     * @param text            text as string
     * @param topNews         boolean as string
     * @param timestamp       timestamp as string
     * @param authorFirstName firstname of author
     * @param authorLastName  lastname of author
     * @param categoryName    name of category
     * @return news entity
     */
    private News buildNewsFromStrings(String title, String text, String topNews, String timestamp, String authorFirstName, String authorLastName, String categoryName) {
        List<Author> authorList = authorRepository.findAuthorByFirstnameAndLastname(authorFirstName, authorLastName);
        Author author = null;
        if (authorList.size() > 0) {
            author = authorList.get(0);
        }
        List<Category> categoryList = categoryRepository.findCategoryByName(categoryName);
        return new News(title, text, Boolean.parseBoolean(topNews), Timestamp.valueOf(timestamp), author, categoryList);
    }

    /**
     * Saves the news from a CSV into the DB
     */
    class AddNewsFromCsvAction implements Action {
        public void execute() {
            String pathToCsv = "news.csv";
            try {
                BufferedReader csvReader = new BufferedReader(new FileReader(pathToCsv));
                String row;

                while ((row = csvReader.readLine()) != null) {

                    String[] data = row.split(",");
                    News news = buildNewsFromStrings(data[0], data[1], data[2], data[3], data[4], data[5], data[6]);
                    newsRepository.save(news);
                }
                csvReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public String toString() {
            return "Add news from CSV: \"news.csv\" ";
        }
    }

    /**
     * Creates a news from the cmd line and saves into the DB
     */
    class AddNewsFromCmdLineAction implements Action {

        @Override
        public void execute() {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Title: ");
            String title = scanner.nextLine();
            System.out.println("Text: ");
            String text = scanner.nextLine();
            System.out.println("TopNews: 1 or 0");
            String topNews = scanner.nextLine();
            System.out.println("Timestamp: yyyy-[m]m-[d]d hh:mm");
            String timestamp = scanner.nextLine();
            System.out.println("Author Firstname: ");
            String firstName = scanner.nextLine();
            System.out.println("Author Lastname: ");
            String lastName = scanner.nextLine();
            System.out.println("Category Name: ");
            String category = scanner.nextLine();

            News news = buildNewsFromStrings(title, text, topNews, timestamp, firstName, lastName, category);
            newsRepository.save(news);
        }

        @Override
        public String toString() {
            return "Add news from Cmdline";
        }
    }

    /**
     * Prints all news with their information
     */
    class ShowNewsAction implements Action {
        public void execute() {
            List<News> newsList = newsRepository.findAll();

            System.out.println("Entries: " + newsList.size());
            for (News ne : newsList) {
                ne.printNews();
            }

        }

        @Override
        public String toString() {
            return "Show news";
        }
    }

    /**
     * Changes an existing news
     */
    class ChangeNewsAction implements Action {

        @Override
        public void execute() {
            Timestamp ts = null;
            Scanner scanner = new Scanner(System.in);

            System.out.println("ID: ");
            String id = scanner.nextLine();

            System.out.println("Title: ");
            String title = scanner.nextLine();

            System.out.println("Text: ");
            String text = scanner.nextLine();

            System.out.println("TopNews: 1 or 0");
            String topNews = scanner.nextLine();

            boolean parsed = false;
            while (!parsed) {
                try {
                    System.out.println("Timestamp: yyyy-[m]m-[d]d hh:mm:ss");
                    String timestamp = scanner.nextLine();
                    ts = Timestamp.valueOf(timestamp);
                    parsed = true;
                } catch (IllegalArgumentException e) {
                    System.out.println("Wrong Format try again!");
                }

            }


            System.out.println("Author ID");
            String authorId = scanner.nextLine();
            Author author = authorRepository.findById(Long.valueOf(authorId));

            System.out.println("Category IDs");
            String categoryIds = scanner.nextLine();
            List<Category> categoryList = new ArrayList<>();
            for (String categoryId : categoryIds.split(",")) {
                categoryList.add(categoryRepository.findById(Long.valueOf(categoryId)));
            }


            News news = new News(Long.valueOf(id), title, text, Boolean.parseBoolean(topNews), ts, author, categoryList);
            newsRepository.save(news);
        }

        @Override
        public String toString() {
            return "Change news";
        }
    }

    /**
     * Delete all categories from the DB
     */
    class DeleteAllNewsAction implements Action {
        public void execute() {
            newsRepository.deleteAll();
        }

        @Override
        public String toString() {
            return "Delete all news";
        }
    }

}
