package at.example.application;

import at.oswald.repository.ComponentScanModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

import java.util.Scanner;

public class MainApp {
    public static final ComponentScanModule componentScanModule = new ComponentScanModule(MainApp.class.getPackageName());
    static final Injector injector = Guice.createInjector(componentScanModule);

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input;
        AuthorManager authorManager = new AuthorManager();
        CategoryManager categoryManager = new CategoryManager();
        NewsManager newsManager = new NewsManager();

        while (true) {
            System.out.println("\n----------------------");
            System.out.println("Choose Actions");
            System.out.println("1 Author Actions");
            System.out.println("2 Category Actions");
            System.out.println("3 News Actions");
            System.out.println("x Stop Program");
            input = scanner.nextLine();

            switch (input) {
                case "1":
                    authorManager.printMenu();
                    break;
                case "2":
                    categoryManager.printMenu();
                    break;
                case "3":
                    newsManager.printMenu();
                    break;
                case "x":
                    System.exit(0);
                    break;
            }
        }

    }
}
