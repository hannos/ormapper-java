package at.example.application;

import at.example.application.entity.Author;
import at.example.application.repository.AuthorRepository;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * Class to handle author entities
 */
public class AuthorManager {
    AuthorRepository authorRepository = MainApp.injector.getInstance(AuthorRepository.class);
    Map<String, Action> actions = new HashMap<>();


    /**
     * Construct an author manager instance
     */
    public AuthorManager() {
        actions.put("1", new ShowAuthorsAction());
        actions.put("2", new AddAuthorFromCmdLineAction());
        actions.put("3", new AddAuthorsFromCsvAction());
        actions.put("4", new ChangeAuthorAction());
        actions.put("5", new DeleteAllAuthorsAction());
    }


    /**
     * Prints the menu for author actions
     */
    public void printMenu() {
        Scanner scanner = new Scanner(System.in);
        String actionKey;

        System.out.println("\n----------------------");
        System.out.println("Author Actions");
        for (String key : actions.keySet()) {
            System.out.println(key + "  " + actions.get(key));
        }
        actionKey = scanner.nextLine();
        if (actions.containsKey(actionKey)) {
            actions.get(actionKey).execute();
        }
    }


    /**
     * Saves the authors from a CSV into the DB
     */
    class AddAuthorsFromCsvAction implements Action {
        String pathToCsv = "authors.csv";

        public void execute() {
            try {
                BufferedReader csvReader = new BufferedReader(new FileReader(pathToCsv));
                String row;

                while ((row = csvReader.readLine()) != null) {

                    String[] data = row.split(",");
                    authorRepository.save(new Author(data[0], data[1]));
                }
                csvReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public String toString() {
            return "Add authors from CSV: " + pathToCsv;
        }
    }

    /**
     * Create an author from the cmd line and save into the DB
     */
    class AddAuthorFromCmdLineAction implements Action {

        @Override
        public void execute() {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Firstname: ");
            String firstName = scanner.nextLine();
            System.out.println("Lastname: ");
            String lastName = scanner.nextLine();

            Author author = new Author(firstName, lastName);

            authorRepository.save(author);
        }

        @Override
        public String toString() {
            return "Add author from cmdline";
        }
    }

    /**
     * Prints all authors with their information
     */
    class ShowAuthorsAction implements Action {
        public void execute() {
            List<Author> authorsList = authorRepository.findAll();

            System.out.println("Entries: " + authorsList.size());
            for (Author author : authorsList) {
                System.out.printf("id: %s firstName: %s lastName: %s%n", author.getId(), author.getFirstName(), author.getLastName());
            }

        }

        @Override
        public String toString() {
            return "Show authors";
        }
    }

    /**
     * Change an existing author
     */
    class ChangeAuthorAction implements Action {

        @Override
        public void execute() {
            Scanner scanner = new Scanner(System.in);

            System.out.println("ID: ");
            String id = scanner.nextLine();

            System.out.println("Firstname: ");
            String firstName = scanner.nextLine();

            System.out.println("Lastname: ");
            String lastName = scanner.nextLine();

            Author author = new Author(Long.valueOf(id), firstName, lastName, null);

            authorRepository.save(author);
        }

        @Override
        public String toString() {
            return "Change author";
        }
    }

    /**
     * Delete all authors from the DB
     */
    class DeleteAllAuthorsAction implements Action {
        public void execute() {
            authorRepository.deleteAll();
        }

        @Override
        public String toString() {
            return "Delete all authors";
        }
    }

}
