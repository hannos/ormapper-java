package at.example.application;

/**
 * Interface for actions
 */
@FunctionalInterface
public interface Action {
    void execute();
}
