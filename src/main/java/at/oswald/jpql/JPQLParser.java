package at.oswald.jpql;

import at.oswald.ormapper.EntityInfo;
import at.oswald.ormapper.MappingInfo;
import at.oswald.ormapper.ReflectionUtility;
import at.oswald.sql.SelectStatement;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * Parses JPQL queries and returns native SQL queries
 */
public class JPQLParser {
    private final Map<Class<?>, EntityInfo> entitiesInfo;
    private final Class<?>[] entities;
    private final SelectStatement statement;
    private Class<?> identificationVariableClass;


    /**
     * Constructor to parse a jpql String
     *
     * @param jpqlString   the jpql query to parse
     * @param entitiesInfo the information about the entities
     */
    public JPQLParser(String jpqlString, Map<Class<?>, EntityInfo> entitiesInfo) {
        this.entitiesInfo = entitiesInfo;
        this.entities = entitiesInfo.keySet().toArray(new Class[0]);

        this.statement = buildNativeQuery(jpqlString);
    }


    /**
     * Returns native SQL for this JPQL query
     *
     * @return native SQL
     */
    public String getNativeSQL() {
        return statement.getSQL();
    }


    /**
     * Returns the tablename for this query
     *
     * @return table for this query
     */
    public String getTableName() {
        return statement.getTableName();
    }


    /**
     * Returns the class of the result
     *
     * @return class of the result of this query
     */
    public Class<?> getReturnType() {
        return identificationVariableClass;
    }


    /**
     * Build a native Query from a jpql query
     *
     * @param jpqlString the jpql query to build the native query from
     * @return the native select statement
     */
    private SelectStatement buildNativeQuery(String jpqlString) {
        List<String> selectClauseTokens = new ArrayList<>();
        List<String> fromClauseTokens = new ArrayList<>();
        List<String> whereClauseTokens = new ArrayList<>();
        List<String> currentList = selectClauseTokens;

        String[] tokens = jpqlString.strip().split(" ");
        for (var t : tokens) {
            switch (t) {
                case "select":
                    currentList = selectClauseTokens;
                    continue;
                case "from":
                    currentList = fromClauseTokens;
                    continue;
                case "where":
                    currentList = whereClauseTokens;
                    continue;
            }
            currentList.add(t);
        }
        if (selectClauseTokens.isEmpty() || fromClauseTokens.isEmpty()) {
            throw new IllegalArgumentException("select or from clause is empty");
        }
        String identificationVariable = parseIdentificationVariable(selectClauseTokens);
        this.identificationVariableClass = findIdentificationVariableClass(identificationVariable, fromClauseTokens);

        SelectStatement selectStatement = SelectStatement.forTable(entitiesInfo.get(identificationVariableClass).getTableName());


        parseWhereClause(selectStatement, whereClauseTokens);

        return selectStatement;

    }


    /**
     * Parse the identification variable from the select clause
     * E.g. select c from Category c where c.name = ?1 -> c is the identification variable
     *
     * @param selectClauseTokens tokens of the selectClause
     * @return identification variable name
     */
    private String parseIdentificationVariable(List<String> selectClauseTokens) {
        return selectClauseTokens.stream()
                .filter(s -> !s.equals("distinct"))
                .collect(Collectors.toList())
                .stream().findAny().orElseThrow(() -> new IllegalArgumentException("Only JPQL queries with 1 identificationVariable supported!"));
    }


    /**
     * Parse the class of the identification variable from the from clause
     *
     * @param identificationVariable name of identification variable
     * @param fromClauseTokens       the tokens of the fromClause
     * @return class of the identification variable
     */
    private Class<?> findIdentificationVariableClass(String identificationVariable, List<String> fromClauseTokens) {

        for (int i = 0; i < fromClauseTokens.size(); i++) {
            String token = fromClauseTokens.get(i);

            if (identificationVariable.equals(token)) {
                return ReflectionUtility.findClassByNameCaseInsensitive(entities, fromClauseTokens.get(i - 1));

            }

        }

        throw new IllegalArgumentException("Could not find class for identificationVariable: " + identificationVariable);

    }


    /**
     * Parse the whereClause and replace the java property names with the database column names
     *
     * @param statement         the statement where to add the where clauses
     * @param whereClauseTokens the tokens of the whereClause
     */
    private void parseWhereClause(SelectStatement statement, List<String> whereClauseTokens) {
        for (var t : whereClauseTokens) {
            if (t.contains(".")) {
                statement.addToWhereClause(convertPropertyNameToColumnName(t));
            } else {
                statement.addToWhereClause(t);
            }
        }

    }


    /**
     * Convert a java property name to a db column name
     *
     * @param propertyName the name of the property
     * @return columnName
     */
    private String convertPropertyNameToColumnName(String propertyName) {
        var split = propertyName.split("\\.");
        assert split.length == 2;
        var variableName = split[0];
        var variableProperty = split[1];

        if (ReflectionUtility.hasClassFieldCaseInsensitive(identificationVariableClass, variableProperty)) {
            final MappingInfo mappingForField = entitiesInfo.get(identificationVariableClass).getMappingForField(variableProperty);
            if (mappingForField == null) {
                throw new IllegalArgumentException(String.format("Query failed! Reason attribute '%s' not found in '%s' Check spelling", variableProperty, identificationVariableClass.getName()));
            }
            return mappingForField.getColumnInfoEntity().getColumnName();
        }
        throw new IllegalArgumentException(String.format("variable %s of type %s has no property %s ", variableName, identificationVariableClass.getName(), variableProperty));
    }

}
