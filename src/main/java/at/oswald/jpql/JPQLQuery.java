package at.oswald.jpql;

import java.lang.annotation.*;

/**
 * Annotation to mark methods with JPQL queries
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.ANNOTATION_TYPE})
@Documented
public @interface JPQLQuery {
    /**
     * Defines the JPQL query to be executed when the annotated method is called.
     *
     * @return the query
     */
    String value();
}
