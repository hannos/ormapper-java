package at.oswald.sql;

/**
 * Class to build native delete statements
 */
public class DeleteStatement {
    private final String tableName;
    private final WhereClause whereClause = new WhereClause();

    /**
     * Constructs a native delete SQL statement
     *
     * @param tableName the name of the table
     */
    private DeleteStatement(String tableName) {
        this.tableName = tableName;
    }

    /**
     * Build a delete statement for a table
     *
     * @param tableName the name of the table
     * @return the delete statement
     */
    public static DeleteStatement forTable(String tableName) {
        return new DeleteStatement(tableName);
    }

    /**
     * Add an equals condition to the whereClause
     *
     * @param columnName        the name of the column for the condition
     * @param parameterPosition the position of the parameter
     * @return the new delete statement
     */
    public DeleteStatement addEqualsToWhereClause(String columnName, int parameterPosition) {
        whereClause.add(String.format("%s = ?%d", columnName, parameterPosition));
        return this;
    }

    /**
     * Return native delete SQL
     *
     * @return native delete SQL
     */
    public String getSQL() {
        return String.format("DELETE FROM `%s` %s", tableName, whereClause);
    }
}
