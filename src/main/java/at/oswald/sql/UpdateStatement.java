package at.oswald.sql;

import at.oswald.ormapper.ColumnInfo;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Class to build native update statements
 * named parameters are used
 */
public class UpdateStatement {
    private final String tableName;
    private final StringBuilder totalSQL = new StringBuilder();


    /**
     * Creates a native update statement with the columnNames as named parameters
     *
     * @param tableName      the name of the table to update
     * @param whereColumn    the column to use in the where part
     * @param columnToUpdate the column to update
     */
    public UpdateStatement(String tableName, ColumnInfo whereColumn, ColumnInfo columnToUpdate) {
        this(tableName, whereColumn, Collections.singletonList(columnToUpdate));
    }

    /**
     * Creates a native update statement with the columnNames as named parameters
     *
     * @param tableName       the name of the table to update
     * @param whereColumn     the column to use in the where part
     * @param columnsToUpdate the columns to update
     */
    public UpdateStatement(String tableName, ColumnInfo whereColumn, List<ColumnInfo> columnsToUpdate) {
        this(tableName, whereColumn,
                columnsToUpdate.stream().collect(Collectors.toMap(ColumnInfo::getColumnName, c -> c))
        );
    }

    /**
     * Creates a native update statement with the map keys as named parameters
     *
     * @param tableName              the name of the table to update
     * @param whereColumn            the column to use in the where part
     * @param parameterColumnInfoMap the columns to update
     */
    public UpdateStatement(String tableName, ColumnInfo whereColumn, Map<String, ColumnInfo> parameterColumnInfoMap) {
        assert !parameterColumnInfoMap.isEmpty();
        this.tableName = tableName;

        String startingPart = String.format("UPDATE `%s` ", tableName);
        totalSQL.append(startingPart)
                .append("SET ")
                .append(makeSetPart(parameterColumnInfoMap))
                .append(" WHERE ")
                .append(makeWherePart(whereColumn));
    }

    /**
     * Return the native update SQL
     *
     * @return the native update SQL
     */
    public String getSQL() {
        return totalSQL.toString();
    }

    /**
     * Make the set part of the update statement
     *
     * @param parameterColumnInfoMap map of parameterNames and columnInfos
     * @return set part of the update statement
     */
    private String makeSetPart(Map<String, ColumnInfo> parameterColumnInfoMap) {
        StringBuilder setPart = new StringBuilder();

        for (String parameterName : parameterColumnInfoMap.keySet()) {
            ColumnInfo columnInfo = parameterColumnInfoMap.get(parameterName);
            if (columnInfo != null && columnInfo.getTableName().equals(tableName)) {
                setPart.append(String.format(" `%s` = :%s,", columnInfo.getColumnName(), parameterName));
            }
        }
        if (setPart.charAt(setPart.length() - 1) == ',') {
            setPart.deleteCharAt(setPart.length() - 1);
        }
        return setPart.toString();
    }

    /**
     * Make the where part of the update statement
     *
     * @param whereColumn the column to use in the where part
     * @return the where part of the update statement
     */
    private String makeWherePart(ColumnInfo whereColumn) {
        return String.format("`%s` = :%s", whereColumn.getColumnName(), whereColumn.getColumnName());
    }


}
