package at.oswald.sql;

import at.oswald.ormapper.ColumnInfo;

import java.util.List;

/**
 * Class to build insert statements
 */
public class InsertStatement {
    private final StringBuilder totalSQL = new StringBuilder();

    /**
     * Constructs a native insert SQL statement
     *
     * @param tableName   the name of the table
     * @param columnInfos the column infos for this table
     */
    public InsertStatement(String tableName, List<ColumnInfo> columnInfos) {
        String attributeNamesPart = makeAttributeNamesPart(columnInfos);
        String valuesPart = makeValuesPart(columnInfos);

        totalSQL.append(String.format("INSERT INTO `%s`", tableName))
                .append(" (")
                .append(attributeNamesPart)
                .append(" ) ")
                .append(" VALUES ( ")
                .append(valuesPart)
                .append(")");

    }

    /**
     * Return the insert sql with named parameters
     * The parameters are named after the column names
     *
     * @return the insert sql with named parameters
     */
    public String getSQL() {
        return totalSQL.toString();
    }

    /**
     * Make the part of the sql with the names of the attributes
     *
     * @param columnInfos the information about the columns
     * @return the part of the sql with the attribute names
     */
    private String makeAttributeNamesPart(List<ColumnInfo> columnInfos) {
        StringBuilder attributeNamesPart = new StringBuilder();

        for (int i = 0; i < columnInfos.size(); i++) {
            if (columnInfos.get(i) != null) {
                if (i > 0) {
                    attributeNamesPart.append(", ");
                }
                attributeNamesPart.append(String.format("`%s`", columnInfos.get(i).getColumnName()));
            }
        }

        return attributeNamesPart.toString();
    }

    /**
     * Make the part of the sql with the values of the attributes
     *
     * @param columnInfos the information about the columns
     * @return the part of the sql with the values as named parameters
     */
    private String makeValuesPart(List<ColumnInfo> columnInfos) {
        StringBuilder valuesPart = new StringBuilder();

        for (int i = 0; i < columnInfos.size(); i++) {
            if (columnInfos.get(i) != null) {
                valuesPart.append(" :").append(columnInfos.get(i).getColumnName());

                if (i != (columnInfos.size() - 1)) {
                    valuesPart.append(",");
                }
            }
        }

        return valuesPart.toString();

    }

}
