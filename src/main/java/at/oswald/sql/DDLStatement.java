package at.oswald.sql;

import at.oswald.ormapper.ColumnInfo;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

/**
 * Class to build DDL statements
 */
public class DDLStatement {

    /**
     * Build a create table if not exists statement
     *
     * @param tableName   the name of the table
     * @param columnInfos the columns of the table
     * @return sql string
     */
    public static String buildCreateTableIfNotExitsStatement(String tableName, List<ColumnInfo> columnInfos) {
        StringBuilder total = new StringBuilder();
        String columnPart = makeColumnPart(tableName, columnInfos);
        String primaryKeyPart = makePrimaryKeyPart(columnInfos);


        total.append(String.format("CREATE TABLE IF NOT EXISTS `%s`", tableName))
                .append(" ( ")
                .append(columnPart)
                .append(primaryKeyPart)
                .append(" );");

        return total.toString();
    }

    /**
     * Make the part of the sql with the columns
     *
     * @param tableName   the name of the table
     * @param columnInfos the columns of the table
     * @return the column part of the create table sql
     */
    private static String makeColumnPart(String tableName, List<ColumnInfo> columnInfos) {
        StringBuilder columnPart = new StringBuilder();

        for (int i = 0; i < columnInfos.size(); i++) {
            var columnInfo = columnInfos.get(i);

            if (columnInfo.getTableName().equals(tableName)) {
                if (i > 0) {
                    columnPart.append(", ");
                }
                columnPart.append(columnInfo.getCreateTableSQL());
            }
        }

        return columnPart.toString();
    }

    /**
     * Make the primary key part of the create table sql
     *
     * @param columnInfos the columns of the table
     * @return sql to create a primary key
     */
    private static String makePrimaryKeyPart(List<ColumnInfo> columnInfos) {
        Optional<ColumnInfo> primaryKeyColumn = columnInfos.stream()
                .filter(ColumnInfo::isPrimaryKey)
                .findFirst();

        if (primaryKeyColumn.isPresent()) {
            return String.format(", PRIMARY KEY (`%s`)", primaryKeyColumn.get().getColumnName());
        } else {
            return "";
        }

    }


    /**
     * Build a sql to add a foreign key constraint
     *
     * @param tableName        the name of the table
     * @param foreignKeyColumn the name of the column for the foreign key
     * @param referenceTable   the table which is referenced by the foreign key
     * @param referenceColumn  the column which is referenced by the foreign key
     * @return sql to add a foreign key constraint
     */
    public static String buildAddForeignKeyStatement(String tableName, String foreignKeyColumn, String referenceTable, String referenceColumn) {
        return String.format("alter table `%s` add constraint foreign key (`%s`) references `%s` (`%s`);", tableName, foreignKeyColumn, referenceTable, referenceColumn);
    }


    /**
     * Return the sql type for a class
     *
     * @param clazz the class
     * @return the sql type for a class
     */
    public static String getSQLTypeForClass(Class<?> clazz) {

        if (clazz == Integer.class || clazz == int.class) {
            return "INT";
        } else if (clazz == Long.class || clazz == long.class || clazz == List.class) {
            return "INT8";
        } else if (clazz == Double.class || clazz == double.class) {
            return "FLOAT";
        } else if (clazz == Boolean.class || clazz == boolean.class) {
            return "BOOLEAN";
        } else if (clazz == String.class) {
            return "VARCHAR";
        } else if (clazz == Timestamp.class) {
            return "TIMESTAMP";
        }

        throw new IllegalArgumentException("No SQL type defined for this class: " + clazz.getSimpleName());
    }


}
