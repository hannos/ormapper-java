package at.oswald.sql;

import lombok.Getter;


/**
 * Class for whereClauses
 */
public class WhereClause {
    @Getter
    private final StringBuilder whereClause = new StringBuilder();

    /**
     * Adds a SQL to the whereClause
     *
     * @param sql the sql to add
     */
    public void add(String sql) {
        if (whereClause.length() == 0) {
            whereClause.append(" WHERE");
        }
        whereClause.append(" ").append(sql);
    }

    @Override
    public String toString() {
        return whereClause.toString();
    }

}
