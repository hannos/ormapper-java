package at.oswald.sql;


import lombok.Getter;

/**
 * Class to build native select statements
 */
public class SelectStatement {
    @Getter
    private final String tableName;
    private final WhereClause whereClause = new WhereClause();

    /**
     * Constructs a native select SQL
     *
     * @param tableName the name of the table
     */
    private SelectStatement(String tableName) {
        this.tableName = tableName;
    }

    /**
     * Build a select statement for a table
     *
     * @param tableName the name of the table
     * @return select statement with this tableName
     */
    public static SelectStatement forTable(String tableName) {
        return new SelectStatement(tableName);
    }

    /**
     * Adds a equals condition to the whereClause
     *
     * @param columnName        the name of the column for the condition
     * @param parameterPosition the parameter position for this condition
     * @return the new select statement
     */
    public SelectStatement addEqualsToWhereClause(String columnName, int parameterPosition) {
        whereClause.add(String.format("%s = ?%d", columnName, parameterPosition));
        return this;
    }

    /**
     * Add an arbitrary sql to the WhereClause
     *
     * @param sqlPart a sql
     * @return the new select statement
     */
    public SelectStatement addToWhereClause(String sqlPart) {
        whereClause.add(sqlPart);
        return this;
    }

    /**
     * Return the native select sql
     *
     * @return the native select sql
     */
    public String getSQL() {
        return String.format("SELECT * FROM `%s` %s", tableName, whereClause)
                .strip()
                .replaceAll("\\s{2,}", " ");
    }

}
