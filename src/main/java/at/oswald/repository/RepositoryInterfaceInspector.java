package at.oswald.repository;

import at.oswald.ormapper.ReflectionUtility;
import lombok.Getter;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.lang.reflect.Field;

import static at.oswald.ormapper.ReflectionUtility.getGenericTypes;

/**
 * Class to check if a repository was defined correctly
 */
public class RepositoryInterfaceInspector {
    private final Class<?> repositoryClass;
    @Getter
    private final Class<?> entityClass;
    @Getter
    private final Class<?> idClass;
    private final int ENTITY_GENERIC_TYPE_INDEX = 0;
    private final int ID_GENERIC_TYPE_INDEX = 1;

    /**
     * Constructs a repository interface inspector
     *
     * @param repositoryClass the class of the repository
     */
    public RepositoryInterfaceInspector(Class<?> repositoryClass) {
        this.repositoryClass = repositoryClass;
        var types = getGenericTypes(repositoryClass);
        entityClass = (Class<?>) types.get(ENTITY_GENERIC_TYPE_INDEX);
        idClass = (Class<?>) types.get(ID_GENERIC_TYPE_INDEX);
    }

    /**
     * Check if the repository was defined correctly
     */
    public void check() {

        if (!entityClass.isAnnotationPresent(Entity.class)) {
            throw new IllegalArgumentException(String.format("Entity of Repository: %s is not annotated with Entity", repositoryClass.getName()));
        }

        for (Field field : entityClass.getDeclaredFields()) {
            if (ReflectionUtility.hasFieldAnnotation(field, Id.class)) {
                if (field.getType() != idClass) {
                    throw new IllegalArgumentException("Id attribute and repository type dont match! " + field.getType() + " " + idClass);
                }
            }
        }

    }


}
