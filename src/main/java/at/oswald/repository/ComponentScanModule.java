package at.oswald.repository;

import at.oswald.ormapper.DBProperties;
import at.oswald.ormapper.EntityManager;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.google.common.reflect.Reflection;
import com.google.inject.AbstractModule;
import org.reflections.Reflections;

import javax.persistence.Entity;
import java.io.File;
import java.io.IOException;
import java.util.Set;


/**
 * Module to enable dependency injection of repositories
 */
public final class ComponentScanModule extends AbstractModule {
    private final String packageName;
    private final String PROPERTIES_PATH = "src/main/resources/database.yml";
    ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
    private DBProperties dbProperties;


    /**
     * Constructs a ComponentScanModule
     *
     * @param packageName the name of the package
     */
    public ComponentScanModule(String packageName) {
        this.packageName = packageName;
        try {
            dbProperties = mapper.readValue(new File(PROPERTIES_PATH), DBProperties.class);
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("Check if " + PROPERTIES_PATH + " exists!");
            System.exit(1);
        }

    }

    /**
     * Binds the repositories to the implementation
     */
    @SuppressWarnings({"unstable", "UnstableApiUsage"})
    @Override
    protected void configure() {
        Reflections packageReflections = new Reflections(packageName);
        var types = packageReflections.getSubTypesOf(BaseJpaRepository.class);
        Set<Class<?>> entitiesClasses = packageReflections.getTypesAnnotatedWith(Entity.class);
        EntityManager entityManager = EntityManager.getInstance(dbProperties, entitiesClasses);
        entityManager.initialize(entitiesClasses);

        types.stream()
                .filter(Class::isInterface)
                .forEach(
                        repoClass -> {
                            BaseJpaRepository<?, ?> proxy = Reflection.newProxy(repoClass, new RepositoryInvocationHandler(repoClass, entityManager));
                            bindObjectAsRaw(repoClass, proxy);
                        }
                );

    }

    /**
     * Suppress warnings to convince the Java compiler to allow the cast.
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    private void bindObjectAsRaw(Class aClass, Object instance) {
        bind(aClass).toInstance(instance);
    }

}
