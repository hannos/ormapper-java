package at.oswald.repository;


import at.oswald.ormapper.EntityManager;
import at.oswald.ormapper.Query;
import at.oswald.sql.SelectStatement;

import java.util.ArrayList;
import java.util.List;

/**
 * Provides the base implementation for data repositories
 *
 * @param <T>  the type of the entity of the repository
 * @param <ID> the type of the id of the entity
 */
public class BaseRepositoryImplementation<T, ID> implements BaseJpaRepository<T, ID> {

    private final Class<T> entityClazz;
    private final Class<ID> idClazz;
    private final EntityManager entityManager;

    /**
     * Constructs a base repository
     *
     * @param entityClazz   the class of the entity of this repository
     * @param idClazz       the class of the ID
     * @param entityManager a entityManager
     */
    public BaseRepositoryImplementation(Class<T> entityClazz, Class<ID> idClazz, EntityManager entityManager) {
        this.entityClazz = entityClazz;
        this.idClazz = idClazz;
        this.entityManager = entityManager;
    }

    /**
     * Saves an entity into the DB
     *
     * @param entity the entity to save
     * @return the saved entity
     */
    @Override
    public T save(T entity) {
        startTransactionIfNotActive();
        entity = entityManager.merge(entity);
        entityManager.getTransaction().commit();

        return entity;
    }

    /**
     * @param entities the entities to save
     * @return the saved entities
     */
    @Override
    public List<T> saveAll(Iterable<T> entities) {
        startTransactionIfNotActive();
        List<T> result = new ArrayList<>();
        for (T entity : entities) {
            result.add(save(entity));
        }

        return result;
    }


    /**
     * Find all entities
     *
     * @return all entities in the DB
     */
    @Override
    public List<T> findAll() {
        startTransactionIfNotActive();
        String tableName = entityManager.entitiesInfo.get(entityClazz).getTableName();
        String sql = SelectStatement.forTable(tableName).getSQL();
        Query<T> query = entityManager.createNativeQuery(sql, entityClazz);


        List<T> result = query.getResultList();
        entityManager.getTransaction().commit();
        return result;
    }

    /**
     * Find an entity by the id
     *
     * @param id the id of the entity to find
     * @return entity with this id
     */
    @Override
    public T findById(ID id) {
        startTransactionIfNotActive();
        T entity = entityManager.find(entityClazz, id);

        entityManager.getTransaction().commit();
        return entity;
    }

    /**
     * Delete an entity from the DB
     *
     * @param entity the entity to delete
     */
    @Override
    public void delete(T entity) {
        startTransactionIfNotActive();
        if (entity == null) {
            throw new IllegalArgumentException("Entity must not be null!");
        }
        entityManager.remove(entity);

        entityManager.getTransaction().commit();
    }

    /**
     * Delete all specified entities from the DB
     *
     * @param entities the entities to delete
     */
    @Override
    public void deleteAll(Iterable<T> entities) {
        startTransactionIfNotActive();
        for (T entity : entities) {
            delete(entity);
        }
    }

    /**
     * Delete all entities of this type
     */
    @Override
    public void deleteAll() {
        List<?> allEntities = findAll();

        startTransactionIfNotActive();
        for (Object entity : allEntities) {
            entityManager.remove(entity);
        }

        entityManager.getTransaction().commit();
    }

    /**
     * Executes a jpql query
     *
     * @param jpqlQuery       a jpql query
     * @param parameterValues the parameter values for this query
     * @return the result of the jpql query
     */
    public Object executeJPQL(String jpqlQuery, Object... parameterValues) {
        Query<?> query = entityManager.createQuery(jpqlQuery, entityClazz);

        for (int i = 0; i < parameterValues.length; i++) {
            query.setParameter(i + 1, parameterValues[i]);
        }

        startTransactionIfNotActive();
        Object result = query.getResultList();

        entityManager.getTransaction().commit();
        return result;
    }

    /**
     * Starts a transaction if no transaction is active
     */
    private void startTransactionIfNotActive() {
        boolean isTransactionActive = entityManager.getTransaction().isActive();
        if (!isTransactionActive) {
            entityManager.getTransaction().begin();
        }
    }

}
