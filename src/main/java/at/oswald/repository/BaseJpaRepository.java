package at.oswald.repository;

import java.util.List;

public interface BaseJpaRepository<T, ID> {

    /**
     * Saves an entity into the DB
     *
     * @param entity the entity to save
     * @return the saved entity
     */
    T save(T entity);

    /**
     * Saves all entities into the DB
     *
     * @param entities the entities to save
     * @return the saved entities
     */
    List<T> saveAll(Iterable<T> entities);

    /**
     * Find all entities
     *
     * @return all entities in the DB
     */
    List<T> findAll();

    /**
     * Find an entity by the id
     *
     * @param id the id of the entity to find
     * @return entity with this id
     */
    T findById(ID id);

    /**
     * Delete an entity from the DB
     *
     * @param entity the entity to delete
     */
    void delete(T entity);

    /**
     * Delete all specified entities from the DB
     *
     * @param entities the entities to delete
     */
    void deleteAll(Iterable<T> entities);

    /**
     * Delete all entities of this type
     */
    void deleteAll();

}
