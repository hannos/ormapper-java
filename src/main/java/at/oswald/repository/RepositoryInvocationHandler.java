package at.oswald.repository;

import at.oswald.jpql.JPQLQuery;
import at.oswald.ormapper.EntityManager;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;


/**
 * Handles the method calls on the repository proxies
 */
public class RepositoryInvocationHandler implements InvocationHandler {
    private final Class<?> clazz;
    private static EntityManager entityManager;

    /**
     * Constructs a repository invocation handler
     *
     * @param clazz         the class of the repository
     * @param entityManager a entityManager instance
     */
    public RepositoryInvocationHandler(Class<?> clazz, EntityManager entityManager) {
        this.clazz = clazz;
        RepositoryInvocationHandler.entityManager = entityManager;
    }

    /**
     * Processes a method invocation on a proxy instance and returns the result
     *
     * @param proxy  the proxy instance that the method was invoked o
     * @param method the Method instance corresponding to the interface method invoked on the proxy instance
     * @param args   an array of objects containing the values of the arguments passed in the method invocation on the proxy instance
     * @return the value to return from the method invocation on the proxy instance.
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) {
        RepositoryInterfaceInspector inspector = new RepositoryInterfaceInspector(this.clazz);
        inspector.check();
        Class<?> entityClass = inspector.getEntityClass();
        Class<?> idClass = inspector.getIdClass();

        try {
            var repo = new BaseRepositoryImplementation(entityClass, idClass, entityManager);
            if (method.isAnnotationPresent(JPQLQuery.class)) {
                var jpaQuery = method.getAnnotation(JPQLQuery.class).value();
                return repo.executeJPQL(jpaQuery, args);
            } else {
                Method baseRepoMethod = repo.getClass().getDeclaredMethod(method.getName(), method.getParameterTypes());
                return baseRepoMethod.invoke(repo, args);
            }
        } catch (ReflectiveOperationException e) {
            e.printStackTrace();
        }

        throw new IllegalArgumentException("Method could not be derived from query!");

    }


}
