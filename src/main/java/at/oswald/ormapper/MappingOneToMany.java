package at.oswald.ormapper;

import at.oswald.sql.SelectStatement;
import at.oswald.sql.UpdateStatement;
import lombok.Getter;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * Class for one to many (1:m) mappings
 */
public class MappingOneToMany implements MappingInfo {
    @Getter
    private final String fieldName;
    @Getter
    private final Class<?> entityClass;
    @Getter
    private final Class<?> relatedEntityClass;
    @Getter
    private final ColumnInfo columnInfoEntity = null;
    @Getter
    private final ColumnInfo columnInfoRelatedEntity;

    /**
     * Constructs a 1:m mapping
     *
     * @param fieldName               the name of the field for this mapping
     * @param entityClass             the class of the entity with this mapping
     * @param relatedEntityClass      the class of the related entity of this mapping
     * @param columnInfoRelatedEntity the column info of the related entity of this mapping
     */
    public MappingOneToMany(String fieldName, Class<?> entityClass, Class<?> relatedEntityClass, ColumnInfo columnInfoRelatedEntity) {
        this.fieldName = fieldName;
        this.entityClass = entityClass;
        this.relatedEntityClass = relatedEntityClass;
        this.columnInfoRelatedEntity = columnInfoRelatedEntity;
    }

    /**
     * Get the value for this mapping from a result set
     *
     * @param resultSet     the result set
     * @param entityManager a entity manager instance
     * @param entityInfo    the entityInfo for the entity, which has this mapping
     * @return the field value for this mapping
     */
    @Override
    public Object getFieldValueFromResultSet(ResultSet resultSet, EntityManager entityManager, EntityInfo entityInfo) {
        try {
            Object entityId = resultSet.getObject(entityInfo.getPrimaryKeyColumn().getColumnName());
            return new LazyList<>(
                    entityClass,
                    () -> getFieldValue(entityId, entityManager),
                    entityManager
            );
        } catch (SQLException e) {
            SQLExceptionHandler.handle(e);
        }
        return null;
    }

    /**
     * Update the values in the table of the related entity
     *
     * @param entity     the entity to update
     * @param entityInfo the info of the entity
     * @param em         the entity manager
     */
    public void updateRelatedTable(Object entity, EntityInfo entityInfo, EntityManager em) {
        Object entityId = entityInfo.getIdValue(entity);

        setRelatedEntriesToNull(entityId, em);
        updateForeignKeysInRelatedTable(entity, entityId, em);
    }

    /**
     * Set the related entries to null
     *
     * @param id the id of the entity
     * @param em the entity manager
     */
    public void setRelatedEntriesToNull(Object id, EntityManager em) {
        EntityInfo relatedEntityInfo = em.entitiesInfo.get(getRelatedEntityClass());
        String relatedTableName = relatedEntityInfo.getTableName();
        final ColumnInfo columnInfoRelatedEntity = getColumnInfoRelatedEntity();

        UpdateStatement updateStatement = new UpdateStatement(relatedTableName, columnInfoRelatedEntity, Map.of("set_parameter", columnInfoRelatedEntity));
        Query<?> updateQuery = em.createNativeQuery(updateStatement.getSQL(), null);
        updateQuery.setParameter("set_parameter", null);
        updateQuery.setParameter(columnInfoRelatedEntity.getColumnName(), id);
        updateQuery.executeUpdate();
    }

    /**
     * Update the values of the foreign key in the related table
     *
     * @param entity   the entity to update
     * @param entityId the id of the entity
     * @param em       the entity manager
     */
    public void updateForeignKeysInRelatedTable(Object entity, Object entityId, EntityManager em) {
        final Class<?> relatedEntityClass = getRelatedEntityClass();
        EntityInfo relatedEntityInfo = em.entitiesInfo.get(relatedEntityClass);
        List<Object> oneToManyMappingsIds = getIds(entity, relatedEntityInfo);
        ColumnInfo relatedEntityPrimaryKeyColumn = relatedEntityInfo.getPrimaryKeyColumn();
        UpdateStatement statement = new UpdateStatement(relatedEntityInfo.getTableName(),
                relatedEntityPrimaryKeyColumn,
                getColumnInfoRelatedEntity());


        if (oneToManyMappingsIds != null) {
            for (var id1 : oneToManyMappingsIds) {
                Query<?> query = em.createNativeQuery(statement.getSQL(), null);
                query.setParameter(relatedEntityInfo.getPrimaryKeyColumn().getColumnName(), id1);
                query.setParameter(getColumnInfoRelatedEntity().getColumnName(), entityId);
                query.executeUpdate();
            }
        }
    }

    /**
     * Get the value for this mapping
     *
     * @param entityId      the id of the entity
     * @param entityManager the entity manager
     * @return the value for this mapping
     */
    private List<?> getFieldValue(Object entityId, EntityManager entityManager) {
        String relatedEntityTableName = entityManager.entitiesInfo.get(relatedEntityClass).getTableName();
        SelectStatement statement = SelectStatement.forTable(relatedEntityTableName)
                .addEqualsToWhereClause(getColumnInfoRelatedEntity().getColumnName(), 1);

        Query<?> selectQuery = entityManager.createNativeQuery(statement.getSQL(), getRelatedEntityClass());
        selectQuery.setParameter(1, entityId);
        return selectQuery.getResultList();

    }

    /**
     * Get the ids of the list
     *
     * @param entity            the entity
     * @param relatedEntityInfo the related entity info
     * @return the ids of the list
     */
    private List<Object> getIds(Object entity, EntityInfo relatedEntityInfo) {
        Object listValue = getFieldValue(entity, relatedEntityInfo);
        if (listValue == null) {
            return null;
        }

        List<Object> ids = new ArrayList<>();
        if (listValue instanceof List<?>) {
            for (var manyToManyElement : (List<?>) listValue) {
                Object id = relatedEntityInfo.getIdValue(manyToManyElement);
                ids.add(id);
            }
        }
        return ids;
    }

}
