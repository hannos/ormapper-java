package at.oswald.ormapper;

import at.oswald.sql.DDLStatement;
import at.oswald.sql.DeleteStatement;
import at.oswald.sql.InsertStatement;
import at.oswald.sql.SelectStatement;
import lombok.Getter;
import lombok.Setter;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * Class for many to many (m:m) mappings
 */
public class MappingManyToMany implements MappingInfo {
    @Getter
    private final String fieldName;
    @Getter
    private final Class<?> entityClass;
    @Getter
    private final Class<?> relatedEntityClass;
    @Getter
    @Setter
    ColumnInfo columnInfoEntity;
    @Getter
    @Setter
    ColumnInfo columnInfoRelatedEntity;
    @Getter
    private final String tableName;

    /**
     * Constructs an m:m mapping
     *
     * @param fieldName               the field with the m:m mapping
     * @param entityClass             the entity with the m:m mapping
     * @param columnInfoEntity        the column info for this entity
     * @param relatedEntityClass      the class of the related entity
     * @param columnInfoRelatedEntity the column info for the related entity
     * @param tableName               the name of the associative table
     */
    public MappingManyToMany(String fieldName, Class<?> entityClass, ColumnInfo columnInfoEntity, Class<?> relatedEntityClass, ColumnInfo columnInfoRelatedEntity, String tableName) {
        this.fieldName = fieldName;
        this.columnInfoEntity = columnInfoEntity;
        this.columnInfoRelatedEntity = columnInfoRelatedEntity;
        this.entityClass = entityClass;
        this.relatedEntityClass = relatedEntityClass;
        this.tableName = tableName;
    }

    /**
     * Get the value for this mapping from a result set
     *
     * @param resultSet     the result set
     * @param entityManager a entity manager instance
     * @param entityInfo    the entityInfo for the entity, which has this mapping
     * @return the field value for this mapping
     */
    @Override
    public Object getFieldValueFromResultSet(ResultSet resultSet, EntityManager entityManager, EntityInfo entityInfo) {
        try {
            Object entityId = resultSet.getObject(entityInfo.getPrimaryKeyColumn().getColumnName());
            return new LazyList<>(
                    entityClass,
                    () -> getFieldValue(entityId, entityManager),
                    entityManager
            );
        } catch (SQLException e) {
            SQLExceptionHandler.handle(e);
        }
        return null;
    }

    /**
     * Return the sql string to create a associative table
     *
     * @return the sql string to create a associative table
     */
    public String getCreateRelatedTablesSQLString() {
        return DDLStatement.buildCreateTableIfNotExitsStatement(tableName,
                List.of(getColumnInfoEntity(),
                        getColumnInfoRelatedEntity()));
    }

    /**
     * Delete the entries with the entityId from the associative table
     *
     * @param entityId the id of the entries to delete
     * @param em       the entity manager
     */
    public void deleteFromAssociativeTable(Object entityId, EntityManager em) {
        DeleteStatement deleteStatement = DeleteStatement.forTable(tableName)
                .addEqualsToWhereClause(getColumnInfoEntity().getColumnName(), 1);

        Query<?> deleteQuery = em.createNativeQuery(deleteStatement.getSQL(), null);
        deleteQuery.setParameter(1, entityId);
        deleteQuery.executeUpdate();
    }

    /**
     * Update the values in the associative table
     *
     * @param entity     the entity to update
     * @param entityInfo the id of the entity
     * @param em         the entity manager
     */
    public void updateAssociativeTable(Object entity, EntityInfo entityInfo, EntityManager em) {
        Object entityId = entityInfo.getIdValue(entity);
        deleteFromAssociativeTable(entityId, em);
        insert(entity, entityId, em);
    }

    /**
     * Insert the values for this mapping into the db
     *
     * @param entity   the entity to insert
     * @param entityId the id of the entity
     * @param em       the entity manager
     */
    public void insert(Object entity, Object entityId, EntityManager em) {
        List<Object> manyToManyMappingsIds = getIds(entity, em.entitiesInfo.get(relatedEntityClass));

        if (manyToManyMappingsIds != null) {
            List<ColumnInfo> associativeTableColumns = List.of(getColumnInfoEntity(), getColumnInfoRelatedEntity());
            InsertStatement insertStatement = new InsertStatement(tableName, associativeTableColumns);

            for (Object id : manyToManyMappingsIds) {
                Query<?> query = em.createNativeQuery(insertStatement.getSQL(), null);

                query.setParameter(getColumnInfoEntity().getColumnName(), entityId);
                query.setParameter(getColumnInfoRelatedEntity().getColumnName(), id);
                query.executeInsert();
            }
        }
    }

    /**
     * Get the sql statement to create the foreign key constraint
     *
     * @param relatedEntityInfo the entityInfo for the related column
     * @return sql statement to create the foreign key constraint
     */
    public String getCreateForeignKeySQLString(EntityInfo relatedEntityInfo) {
        String foreignKey = getColumnInfoRelatedEntity().getColumnName();
        String destination_table = relatedEntityInfo.getTableName();
        String destination_key = relatedEntityInfo.getPrimaryKeyColumn().getColumnName();
        return DDLStatement.buildAddForeignKeyStatement(tableName, foreignKey, destination_table, destination_key);
    }

    /**
     * Get the value for this mapping
     *
     * @param entityId      the id of the entity
     * @param entityManager the entity manager
     * @return the value for this mapping
     */
    private List<Object> getFieldValue(Object entityId, EntityManager entityManager) {
        List<Map<String, Object>> associativeTable = getAssociativeTableValues(entityId, entityManager);
        assert associativeTable != null;

        final EntityInfo relatedEntityInfo = entityManager.entitiesInfo.get(getRelatedEntityClass());
        String tableName = relatedEntityInfo.getTableName();
        SelectStatement statement = SelectStatement.forTable(tableName)
                .addEqualsToWhereClause(relatedEntityInfo.getPrimaryKeyColumn().getColumnName(), 1);
        Query<?> query = entityManager.createNativeQuery(statement.getSQL(), getRelatedEntityClass());

        List<Object> relatedEntities = new ArrayList<>();
        String relatedColumnName = getColumnInfoRelatedEntity().getColumnName();
        for (var associativeTableRow : associativeTable) {
            Object relatedEntityId = associativeTableRow.get(relatedColumnName);
            query.setParameter(1, relatedEntityId);
            relatedEntities.add(query.getSingleResult());
        }
        return relatedEntities;
    }

    /**
     * Get the rows of the associative table with this entityId
     *
     * @param entityId      the id of the entity
     * @param entityManager the entity manager
     * @return list of maps with the columnNames and their values
     */
    private List<Map<String, Object>> getAssociativeTableValues(Object entityId, EntityManager entityManager) {
        SelectStatement statement = SelectStatement.forTable(tableName)
                .addEqualsToWhereClause(getColumnInfoEntity().getColumnName(), 1);

        Query<?> associativeTableQuery = entityManager.createNativeQuery(statement.getSQL(), null);
        associativeTableQuery.setParameter(1, entityId);
        return associativeTableQuery.getValuesForColumns(List.of(getColumnInfoEntity(), getColumnInfoRelatedEntity()));
    }

    /**
     * Get the ids of the list
     *
     * @param entity            the entity
     * @param relatedEntityInfo the related entity info
     * @return the ids of the list
     */
    private List<Object> getIds(Object entity, EntityInfo relatedEntityInfo) {
        Object listValue = getFieldValue(entity, relatedEntityInfo);
        if (listValue == null) {
            return null;
        }

        List<Object> ids = new ArrayList<>();
        if (listValue instanceof List<?>) {
            for (var manyToManyElement : (List<?>) listValue) {
                Object id = relatedEntityInfo.getIdValue(manyToManyElement);
                ids.add(id);
            }
        }
        return ids;
    }


}
