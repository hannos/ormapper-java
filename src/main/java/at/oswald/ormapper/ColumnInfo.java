package at.oswald.ormapper;

import lombok.Getter;
import lombok.Setter;


/**
 * Contains information about a column
 */
public class ColumnInfo {
    @Getter
    @Setter
    private String tableName;
    @Getter
    private String columnName;
    @Setter
    @Getter
    private String dataType;
    @Setter
    @Getter
    private Integer length;
    @Setter
    @Getter
    private boolean isPrimaryKey;
    @Setter
    private boolean isAutoIncrement;

    /**
     * Constructs a column info class
     *
     * @param tableName  the name of the table
     * @param columnName the name of the column
     * @param dataType   the SQL data type of the column
     */
    public ColumnInfo(String tableName, String columnName, String dataType) {
        this.tableName = tableName;
        setColumnName(columnName);
        this.dataType = dataType;
    }

    /**
     * Constructs a column info class
     *
     * @param tableName       the name of the table
     * @param columnName      the name of the column
     * @param dataType        the SQL data type of the column
     * @param length          the max. length of the column
     * @param isPrimaryKey    is the column the primary key
     * @param isAutoIncrement is the column auto increment
     */
    public ColumnInfo(String tableName, String columnName, String dataType, Integer length, boolean isPrimaryKey, boolean isAutoIncrement) {
        this.tableName = tableName;
        setColumnName(columnName);
        this.dataType = dataType;
        this.length = length;
        this.isPrimaryKey = isPrimaryKey;
        this.isAutoIncrement = isAutoIncrement;
    }

    /**
     * Sets the columnName
     *
     * @param columnName the columnName to set
     */
    public void setColumnName(String columnName) {
        this.columnName = columnName.toLowerCase();
    }

    /**
     * Returns the DDL SQL to create this column
     *
     * @return DDL SQL to create this column
     */
    public String getCreateTableSQL() {
        String sqlAutoIncrement = isAutoIncrement ? "AUTO_INCREMENT" : "";
        String length = this.dataType.equals("VARCHAR") ? String.format("(%s)", this.length) : "";
        return String.format(" `%s` %s%s %s", this.columnName, this.dataType, length, sqlAutoIncrement);
    }
}
