package at.oswald.ormapper;

import javax.persistence.PersistenceException;
import javax.persistence.RollbackException;
import java.sql.SQLException;


/**
 * Class for transactions
 */
public class EntityTransaction {
    EntityManager em;


    /**
     * Construct an entityTransaction
     *
     * @param em an entityManager instance
     */
    public EntityTransaction(EntityManager em) {
        this.em = em;
    }

    /**
     * Start a transaction.
     *
     * @throws IllegalStateException if <code>isActive()</code> is true
     */
    public void begin() {
        if (em.isConnectionActive()) {
            throw new IllegalStateException("transaction already active!");
        }
        em.getConnection();
    }

    /**
     * Commit the current transaction, writing any unflushed changes to the database.
     *
     * @throws IllegalStateException if <code>isActive()</code> is false
     */
    public void commit() {
        if (!isActive()) {
            throw new IllegalStateException("no transaction active!");
        }
        try {
            em.getConnection().commit();
            em.closeConnection();
        } catch (SQLException e) {
            throw new RollbackException(e);
        }
    }

    /**
     * Roll back the current transaction.
     *
     * @throws IllegalStateException if <code>isActive()</code> is false
     * @throws PersistenceException  if an unexpected error
     *                               condition is encountered
     */
    public void rollback() {
        if (!isActive()) {
            throw new IllegalStateException("no transaction active!");
        }
        try {
            em.getConnection().rollback();
            em.closeConnection();
        } catch (SQLException e) {
            SQLExceptionHandler.handle(e);
        }
    }


    /**
     * Indicate whether a resource transaction is in progress.
     *
     * @return boolean indicating whether transaction is in progress
     */
    public boolean isActive() {
        return em.isConnectionActive();
    }
}
