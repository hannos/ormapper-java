package at.oswald.ormapper;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Utility class for reflection
 */
public class ReflectionUtility {

    /**
     * Get the generic types of a class
     *
     * @param clazz the class
     * @return list of types
     */
    public static List<Type> getGenericTypes(Class<?> clazz) {
        List<Type> types = new ArrayList<>();

        Type[] genericInterfaces = clazz.getGenericInterfaces();
        for (Type genericInterface : genericInterfaces) {
            if (genericInterface instanceof ParameterizedType) {
                Type[] genericTypes = ((ParameterizedType) genericInterface).getActualTypeArguments();
                types.addAll(Arrays.asList(genericTypes));
            }
        }

        return types;

    }

    /**
     * Get the first generic type of a field
     *
     * @param field the field
     * @return first generic type of a field
     */
    public static Class<?> getGenericType(Field field) {
        ParameterizedType stringListType = (ParameterizedType) field.getGenericType();
        return (Class<?>) stringListType.getActualTypeArguments()[0];
    }


    /**
     * Get the value of a field from an arbitrary object
     *
     * @param object    the object
     * @param fieldName the name of the field
     * @return the value of the field or null if the object does not have a field with this name
     */
    public static Object getFieldValueFromObject(Object object, String fieldName) {
        try {
            Field field = object.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            return field.get(object);
        } catch (ReflectiveOperationException e) {
            System.err.printf("Field %s not found in object %s%n", fieldName, object);
            e.printStackTrace();
        }

        return null;

    }

    /**
     * Set a value for a field in an object
     *
     * @param object    the object
     * @param fieldName the name of the field to set
     * @param value     the value to set
     */
    public static void setFieldValueInObject(Object object, String fieldName, Object value) {
        try {
            Field field = object.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            field.set(object, value);
        } catch (ReflectiveOperationException e) {
            e.printStackTrace();
        }

    }

    /**
     * Find a class by name case-insensitive
     *
     * @param classes the classes
     * @param name    the name of the class to find
     * @return the class
     */
    public static Class<?> findClassByNameCaseInsensitive(Class<?>[] classes, String name) {
        Optional<Class<?>> result = Arrays.stream(classes)
                .filter(e -> e.getSimpleName().equalsIgnoreCase(name))
                .findFirst();

        return result.orElseThrow(() -> new IllegalArgumentException("Could not find class for " + name));
    }

    /**
     * Checks if the class has a field with this name
     *
     * @param clazz     the class
     * @param fieldName the name of the field to find
     * @return true if the class has this field
     */
    public static boolean hasClassFieldCaseInsensitive(Class<?> clazz, String fieldName) {
        return Arrays.stream(clazz.getDeclaredFields())
                .map((f) -> f.getName().toLowerCase())
                .anyMatch((s) -> s.equals(fieldName.toLowerCase()));
    }

    /**
     * Checks if a field has an annotation
     *
     * @param field      the field
     * @param annotation the annotation to check
     * @return true if the field has the annotation
     */
    public static boolean hasFieldAnnotation(Field field, Class<? extends Annotation> annotation) {
        return field.getAnnotation(annotation) != null;
    }


    /**
     * Find a field with a annotation in a class
     *
     * @param clazz      the class
     * @param annotation the annotation to find
     * @return the field with the annotation
     */
    public static Field findFieldWithAnnotation(Class<?> clazz, Class<? extends Annotation> annotation) {
        Optional<Field> optionalField = Arrays.stream(clazz.getDeclaredFields())
                .filter(field -> field.isAnnotationPresent(annotation))
                .findAny();

        return optionalField.orElseThrow(
                () -> new IllegalArgumentException("Annotation: " + annotation.getName() + " not found!")
        );
    }
}
