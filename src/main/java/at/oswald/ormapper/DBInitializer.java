package at.oswald.ormapper;

import at.oswald.helper.DDLExecutor;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;

/**
 * Class to create and execute the DDL statements for entities
 */
public class DBInitializer {
    private final Map<Class<?>, EntityInfo> entitiesInfo;
    private final DDLExecutor ddlExecutor;

    /**
     * Creates and executes the DDL statements for the entities
     *
     * @param dbProperties the properties of the DB, where to execute the DDL statements
     * @param entitiesInfo the information about the entitites
     */
    public static void initialize(DBProperties dbProperties, Map<Class<?>, EntityInfo> entitiesInfo) {
        DBInitializer dbInitializer = new DBInitializer(
                entitiesInfo,
                new DDLExecutor(dbProperties)
        );
        dbInitializer.createTables();
        dbInitializer.createAssociativeTables();
        dbInitializer.createForeignKeys();

    }

    /**
     * Constructs a DB initializer instance
     *
     * @param entitiesInfo the information about the entites
     * @param ddlExecutor  a ddl executor instance
     */
    private DBInitializer(Map<Class<?>, EntityInfo> entitiesInfo, DDLExecutor ddlExecutor) {
        this.entitiesInfo = entitiesInfo;
        this.ddlExecutor = ddlExecutor;
    }

    /**
     * Create the tables for all entities
     */
    private void createTables() {
        entitiesInfo.values()
                .forEach(entityInfo -> ddlExecutor.createTableIfNotExists(entityInfo.getTableName(), entityInfo.getColumnInfos()));
    }

    /**
     * Creates the associative tables for the m:m relations
     */
    private void createAssociativeTables() {
        entitiesInfo.values().stream()
                .map(EntityInfo::getManyToManyMappings)
                .flatMap(Collection::stream)
                .map(MappingManyToMany::getCreateRelatedTablesSQLString)
                .forEach(ddlExecutor::executeDDLSQL);
    }


    /**
     * Creates the foreignKeys for the m:m relations
     */
    private void createForeignKeys() {
        entitiesInfo.values().stream()
                .map(EntityInfo::getManyToManyMappings)
                .flatMap(Collection::stream)
                .map(mappingInfo -> mappingInfo.getCreateForeignKeySQLString(entitiesInfo.get(mappingInfo.getRelatedEntityClass())))
                .filter(Objects::nonNull)
                .forEach(ddlExecutor::executeDDLSQL);
        entitiesInfo.values().stream()
                .map(EntityInfo::getManyToOneMappings)
                .flatMap(Collection::stream)
                .map(mappingInfo -> mappingInfo.getCreateForeignKeySQLString(entitiesInfo.get(mappingInfo.getEntityClass()), entitiesInfo.get(mappingInfo.getRelatedEntityClass())))
                .filter(Objects::nonNull)
                .forEach(ddlExecutor::executeDDLSQL);
    }

}
