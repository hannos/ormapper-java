package at.oswald.ormapper;

import lombok.Getter;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Class for basic mappings
 */
public class MappingBasic implements MappingInfo {

    @Getter
    private final String fieldName;
    @Getter
    private final Class<?> entityClass;
    @Getter
    private final Class<?> relatedEntityClass = null;
    @Getter
    private final ColumnInfo columnInfoEntity;
    @Getter
    private final ColumnInfo columnInfoRelatedEntity = null;

    /**
     * Constructs a mapping for a basic column/field
     *
     * @param fieldName        the name of the field for the mapping
     * @param entityClass      the entity containing this field
     * @param columnInfoEntity the column info for this mapping and entity
     */
    public MappingBasic(String fieldName, Class<?> entityClass, ColumnInfo columnInfoEntity) {
        this.fieldName = fieldName;
        this.entityClass = entityClass;
        this.columnInfoEntity = columnInfoEntity;
    }

    /**
     * Get the value for this mapping from a result set
     *
     * @param resultSet     the result set
     * @param entityManager a entity manager instance
     * @param entityInfo    the entityInfo for the entity, which has this mapping
     * @return the field value for this mapping
     */
    @Override
    public Object getFieldValueFromResultSet(ResultSet resultSet, EntityManager entityManager, EntityInfo entityInfo) {
        try {
            return resultSet.getObject(getColumnInfoEntity().getColumnName());
        } catch (SQLException e) {
            SQLExceptionHandler.handle(e);
        }
        return null;
    }


}
