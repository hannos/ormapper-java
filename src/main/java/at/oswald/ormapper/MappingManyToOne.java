package at.oswald.ormapper;


import at.oswald.sql.DDLStatement;
import lombok.Getter;

import java.sql.ResultSet;
import java.sql.SQLException;

import static at.oswald.ormapper.ReflectionUtility.getFieldValueFromObject;

/**
 * Class for many to one (m:1) mappings
 */
public class MappingManyToOne implements MappingInfo {
    @Getter
    private final String fieldName;
    @Getter
    private final Class<?> entityClass;
    @Getter
    private final Class<?> relatedEntityClass;
    @Getter
    private final ColumnInfo columnInfoEntity;
    @Getter
    private final ColumnInfo columnInfoRelatedEntity = null;

    /**
     * Constructs a m:1 mapping
     *
     * @param fieldName          the field with the m:1 mapping
     * @param entityClass        the class of the entity with this m:1 mapping
     * @param columnInfoEntity   the column info for this mapping
     * @param relatedEntityClass the class of the related entity
     */
    public MappingManyToOne(String fieldName, Class<?> entityClass, ColumnInfo columnInfoEntity, Class<?> relatedEntityClass) {
        this.fieldName = fieldName;
        this.entityClass = entityClass;
        this.columnInfoEntity = columnInfoEntity;
        this.relatedEntityClass = relatedEntityClass;
    }


    /**
     * Get the value of a entity with this mapping
     *
     * @param entity            the entity with the value
     * @param relatedEntityInfo information about the related entity
     * @return the value to get
     */
    @Override
    public Object getFieldValue(Object entity, EntityInfo relatedEntityInfo) {
        Object fieldValue = null;

        // 1. Get related entity
        Object relatedEntity = getFieldValueFromObject(entity, fieldName);
        // 2. Get ID of related entity
        if (relatedEntity != null) {
            fieldValue = relatedEntityInfo.getIdValue(relatedEntity);
        }
        return fieldValue;
    }

    /**
     * Get the value for this mapping from a result set
     *
     * @param resultSet     the result set
     * @param entityManager a entity manager instance
     * @param entityInfo    the entityInfo for the entity, which has this mapping
     * @return the field value for this mapping
     */
    @Override
    public Object getFieldValueFromResultSet(ResultSet resultSet, EntityManager entityManager, EntityInfo entityInfo) {
        Object att = null;
        try {
            att = resultSet.getObject(getColumnInfoEntity().getColumnName());
        } catch (SQLException e) {
            SQLExceptionHandler.handle(e);
        }

        if (att == null) {
            return null;
        }
        return entityManager.find(getRelatedEntityClass(), att);
    }

    /**
     * Get the sql statement to create the foreign key constraint
     *
     * @param entityInfo        the entityInfo for this entity
     * @param relatedEntityInfo the entityInfo for the related column
     * @return sql statement to create the foreign key constraint
     */
    public String getCreateForeignKeySQLString(EntityInfo entityInfo, EntityInfo relatedEntityInfo) {
        String tableName = entityInfo.getTableName();
        String foreignKey = columnInfoEntity.getColumnName();
        String destination_table = relatedEntityInfo.getTableName();
        String destination_key = relatedEntityInfo.getPrimaryKeyColumn().getColumnName();
        return DDLStatement.buildAddForeignKeyStatement(tableName, foreignKey, destination_table, destination_key);
    }
}
