package at.oswald.ormapper;

import lombok.AllArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Parameter;

/**
 * Type for query parameter objects
 *
 * @param <T> the type of the parameter
 */
@AllArgsConstructor
@ToString
public class ParameterImpl<T> implements Parameter<T> {
    private final String name;
    @Setter
    private Integer position;
    private final Class<T> type;

    /**
     * Return the parameter name, or null if the parameter is not a named parameter or no name has been assigned.
     *
     * @return the parameter name, or null if the parameter is not a named parameter or no name has been assigned.
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Return the parameter position, or null if the parameter is not a positional parameter.
     *
     * @return the parameter position, or null if the parameter is not a positional parameter.
     */
    @Override
    public Integer getPosition() {
        return position;
    }

    /**
     * Return the Java type of the parameter.
     *
     * @return the Java type of the parameter.
     */
    @Override
    public Class<T> getParameterType() {
        return type;
    }


}
