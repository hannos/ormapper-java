package at.oswald.ormapper;


import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * Contains information about a entity class
 */
public class EntityInfo {
    @Getter
    private String tableName;
    @Getter
    private final List<MappingInfo> mappingInfos = new ArrayList<>();
    @Getter
    private MappingInfo primaryKeyMapping;
    @Getter
    private final Class<?> entityClass;

    /**
     * Construcs an entity info
     *
     * @param entityClass the class of the entity
     */
    private EntityInfo(Class<?> entityClass) {
        this.entityClass = entityClass;
    }

    /**
     * Creates an entityInfo from an entityClass
     *
     * @param entityClass the class of the entity
     * @return entityInfo for an entityClass
     */
    public static EntityInfo createFromEntityClass(Class<?> entityClass) {
        List<MappingInfo> mappingInfos = new ArrayList<>();

        var fields = entityClass.getDeclaredFields();
        for (var field : fields) {
            MappingInfo mappingInfo = MappingFactory.createMapping(field);
            mappingInfos.add(mappingInfo);
        }

        return createFromEntityClass(entityClass, mappingInfos);
    }

    /**
     * Creates an entityInfo from an entityClass
     *
     * @param entityClass  the class of the entity
     * @param mappingInfos the mappingInfos for this entity
     * @return entityInfo for an entityClass
     */
    public static EntityInfo createFromEntityClass(Class<?> entityClass, List<MappingInfo> mappingInfos) {
        EntityInfo entityInfo = new EntityInfo(entityClass);

        for (var mappingInfo : mappingInfos) {
            entityInfo.addMappingInfo(mappingInfo);
        }
        if (entityInfo.primaryKeyMapping == null) {
            throw new IllegalStateException("Entity must have a single primary Key");
        }
        entityInfo.tableName = entityInfo.getPrimaryKeyColumn().getTableName();

        return entityInfo;
    }

    /**
     * Add a mappingInfo to this entityInfo
     *
     * @param mappingInfo the mappingInfo to add
     */
    private void addMappingInfo(MappingInfo mappingInfo) {
        mappingInfos.add(mappingInfo);
        if (mappingInfo.getColumnInfoEntity() != null && mappingInfo.getColumnInfoEntity().isPrimaryKey()) {
            primaryKeyMapping = mappingInfo;
        }
    }

    /**
     * Get the primaryKey column for this entity
     *
     * @return primaryKey column for this entity
     */
    public ColumnInfo getPrimaryKeyColumn() {
        return primaryKeyMapping.getColumnInfoEntity();
    }


    /**
     * Get the mappingInfo for a field
     *
     * @param fieldName the name of the field
     * @return the mappingInfo for a field
     */
    public MappingInfo getMappingForField(String fieldName) {
        return mappingInfos.stream()
                .filter(mappingInfo -> mappingInfo.getFieldName().equals(fieldName))
                .findFirst()
                .orElseThrow();
    }

    /**
     * Get all non primaryKey columns for this entity
     *
     * @return all non primaryKey columns for this entity
     */
    public List<ColumnInfo> getNonPrimaryKeyColumns() {
        return mappingInfos.stream()
                .map(MappingInfo::getColumnInfoEntity)
                .filter(Objects::nonNull)
                .filter((columnInfo -> !columnInfo.isPrimaryKey()))
                .collect(Collectors.toList());
    }

    /**
     * Get all columnInfos for this entity
     *
     * @return all columnInfos for this entity
     */
    public List<ColumnInfo> getColumnInfos() {
        return mappingInfos.stream()
                .map(MappingInfo::getColumnInfoEntity)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    /**
     * Get all related EntityClasses for this entity
     *
     * @return all related EntityClasses for this entity
     */
    public Set<Class<?>> getRelatedEntities() {
        return mappingInfos.stream()
                .map(MappingInfo::getRelatedEntityClass)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
    }

    /**
     * Get the id value for an instance of this entity
     *
     * @param entity the entity with the id
     * @return the id of the entity
     */
    public Object getIdValue(Object entity) {
        return primaryKeyMapping.getFieldValue(entity, null);
    }

    /**
     * Get all basic and manyToOne mappings for this entity
     *
     * @return all basic and manyToOne mappings for this entity
     */
    public List<MappingInfo> getBasicAndManyToOneMappings() {
        return getMappingInfos().stream()
                .filter(mappingInfo -> (mappingInfo instanceof MappingBasic || mappingInfo instanceof MappingManyToOne))
                .collect(Collectors.toList());
    }

    /**
     * Get all manyToOne mappings for this entity
     *
     * @return all manyToOne mappings for this entity
     */
    public List<MappingManyToOne> getManyToOneMappings() {
        return getMappingInfos().stream()
                .filter(mappingInfo -> (mappingInfo instanceof MappingManyToOne))
                .map(MappingManyToOne.class::cast)
                .collect(Collectors.toList());
    }

    /**
     * Get all oneToMany mappings for this entity
     *
     * @return all oneToMany mappings for this entity
     */
    public List<MappingOneToMany> getOneToManyMappings() {
        return getMappingInfos().stream()
                .filter(mappingInfo -> (mappingInfo instanceof MappingOneToMany))
                .map(MappingOneToMany.class::cast)
                .collect(Collectors.toList());
    }

    /**
     * Get all manyToMany mappings for this entity
     *
     * @return all manyToMany mappings for this entity
     */
    public List<MappingManyToMany> getManyToManyMappings() {
        return getMappingInfos().stream()
                .filter(mappingInfo -> (mappingInfo instanceof MappingManyToMany))
                .map(MappingManyToMany.class::cast)
                .collect(Collectors.toList());
    }

}
