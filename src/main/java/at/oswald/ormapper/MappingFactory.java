package at.oswald.ormapper;

import at.oswald.sql.DDLStatement;

import javax.persistence.*;
import java.lang.reflect.Field;

/**
 * Factory to create mappings from fields
 */
public class MappingFactory {

    /**
     * Create a mapping from a field
     *
     * @param field the field
     * @return mapping for this field
     */
    public static MappingInfo createMapping(Field field) {
        MappingInfo mappingInfo;
        Class<?> entityClass = field.getDeclaringClass();
        final String fieldName = field.getName();


        if (ReflectionUtility.hasFieldAnnotation(field, ManyToMany.class)) {
            mappingInfo = getMappingInfoForManyToMany(field, entityClass, fieldName);

        } else if (ReflectionUtility.hasFieldAnnotation(field, OneToMany.class)) {
            mappingInfo = getMappingInfoForOneToMany(field, entityClass, fieldName);

        } else if (ReflectionUtility.hasFieldAnnotation(field, ManyToOne.class)) {
            ColumnInfo column = new ColumnInfo(getTableNameForEntity(entityClass), getForeignKeyColumnName(field), getDataTypeForForeignKeys());
            mappingInfo = new MappingManyToOne(fieldName, entityClass, column, field.getType());

        } else {
            ColumnInfo column = getColumnInfoForBasicField(field);
            mappingInfo = new MappingBasic(fieldName, entityClass, column);
        }

        return mappingInfo;

    }


    /**
     * Build a mappingInfo for a 1:m relation field
     *
     * @param field       a 1:m relation field
     * @param entityClass the class of the entity with this field
     * @param fieldName   the name of the field
     * @return the mappingInfo for a 1:m relation field
     */
    private static MappingInfo getMappingInfoForOneToMany(Field field, Class<?> entityClass, String fieldName) {
        Class<?> relatedEntityClass = ReflectionUtility.getGenericType(field);
        String relatedEntityFieldName = field.getDeclaredAnnotation(OneToMany.class).mappedBy();
        String relatedEntityColumnName = getForeignKeyColumnName(relatedEntityFieldName);
        String tableName = getTableNameForEntity(relatedEntityClass);
        ColumnInfo columnRelatedEntity = new ColumnInfo(tableName, relatedEntityColumnName, getDataTypeForForeignKeys());

        return new MappingOneToMany(fieldName, entityClass, relatedEntityClass, columnRelatedEntity);

    }


    /**
     * Build a mappingInfo for a m:m relation field
     *
     * @param field       a m:m relation field
     * @param entityClass the class of the entity with this field
     * @param fieldName   the name of the field
     * @return the mappingInfo for a m:m relation field
     */
    private static MappingInfo getMappingInfoForManyToMany(Field field, Class<?> entityClass, String fieldName) {
        Class<?> relatedEntityClass = ReflectionUtility.getGenericType(field);
        String tableNameForAssociativeTable = getTableNameForAssociativeTable(entityClass, relatedEntityClass);
        ColumnInfo columnEntity = new ColumnInfo(tableNameForAssociativeTable, getForeignKeyColumnName(entityClass), getDataTypeForForeignKeys());
        ColumnInfo columnRelatedEntity = new ColumnInfo(tableNameForAssociativeTable, getForeignKeyColumnName(relatedEntityClass), getDataTypeForForeignKeys());

        return new MappingManyToMany(fieldName, entityClass, columnEntity, relatedEntityClass, columnRelatedEntity, tableNameForAssociativeTable);
    }

    /**
     * Get the columnInfo for a field with a basic mapping
     *
     * @param field the field with a basic mapping
     * @return columnInfo
     */
    private static ColumnInfo getColumnInfoForBasicField(Field field) {
        ColumnInfo columnInfo = new ColumnInfo(getTableNameForEntity(field.getDeclaringClass()),
                getColumnName(field),
                DDLStatement.getSQLTypeForClass(field.getType())
        );

        if (field.isAnnotationPresent(Column.class)) {
            Column column = field.getAnnotation(Column.class);
            columnInfo.setLength(column.length());
        }

        if (ReflectionUtility.hasFieldAnnotation(field, Id.class)) {
            columnInfo.setPrimaryKey(true);
            columnInfo.setAutoIncrement(true);
        }

        return columnInfo;
    }

    /**
     * Get the datatype for foreign key columns
     *
     * @return the datatype for foreign key columns
     */
    private static String getDataTypeForForeignKeys() {
        return DDLStatement.getSQLTypeForClass(Long.class);
    }


    /**
     * Get the column name for a field
     *
     * @param field a field
     * @return the column name for a field
     */
    private static String getColumnName(Field field) {
        if (field.isAnnotationPresent(Column.class)) {
            Column column = field.getAnnotation(Column.class);
            if (!column.name().isEmpty()) {
                return column.name();
            }
        }
        return field.getName().toLowerCase();
    }

    /**
     * Get the name for a foreign Key
     *
     * @param entityClass the class of the entity for the foreign key
     * @return column name
     */
    private static String getForeignKeyColumnName(Class<?> entityClass) {
        return getForeignKeyColumnName(entityClass.getSimpleName().toLowerCase());
    }

    /**
     * Get the name for a foreign Key
     *
     * @param field the field of entity for the foreign key
     * @return column name
     */
    private static String getForeignKeyColumnName(Field field) {
        return getForeignKeyColumnName(field.getName());
    }

    /**
     * Get the name for a foreign Key
     *
     * @param fieldName name of the field of the entity for the foreign key
     * @return column name
     */
    private static String getForeignKeyColumnName(String fieldName) {
        return fieldName + "_id";
    }

    /**
     * Get the table name for an entity
     *
     * @param entityClass the class of the entity
     * @return table name
     */
    private static String getTableNameForEntity(Class<?> entityClass) {
        if (entityClass.isAnnotationPresent(Table.class)) {
            return entityClass.getAnnotation(Table.class).name();
        }
        return entityClass.getSimpleName().toLowerCase();
    }

    /**
     * Get the name of the associative table for the m:m relation
     *
     * @param entityClass        the class of entity
     * @param relatedEntityClass the class of the related entity
     * @return name of the table
     */
    private static String getTableNameForAssociativeTable(Class<?> entityClass, Class<?> relatedEntityClass) {
        String entityTableName = getTableNameForEntity(entityClass);
        String relatedEntityTableName = getTableNameForEntity(relatedEntityClass);

        if (entityTableName.compareTo(relatedEntityTableName) <= 0) {
            return entityTableName + "_" + relatedEntityTableName;
        } else {
            return relatedEntityTableName + "_" + entityTableName;
        }

    }

}
