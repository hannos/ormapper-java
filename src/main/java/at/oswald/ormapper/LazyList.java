package at.oswald.ormapper;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;


/**
 * LazyList based on ArrayList
 *
 * @param <T> the type of the elements
 */
public class LazyList<T> extends ArrayList<T> {
    Class<T> type;
    private final EntityManager em;
    Supplier<?> lazyLoader;
    boolean loaded = false;

    /**
     * Constructs a lazy list
     *
     * @param type       the type of the list
     * @param lazyLoader supplier for loading the result
     * @param em         an entityManager instance
     */
    public LazyList(Class<T> type, Supplier<List<?>> lazyLoader, EntityManager em) {
        this.type = type;
        this.lazyLoader = lazyLoader;
        this.em = em;
    }

    /**
     * Loads the data from the DB
     */
    public void load() {
        boolean newTransactionStarted = false;
        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
            newTransactionStarted = true;
        }
        List<? extends T> loadedAttributes = (List<? extends T>) lazyLoader.get();
        if (newTransactionStarted) {
            em.getTransaction().commit();
        }
        super.addAll(loadedAttributes);
        loaded = true;
    }


    /**
     * Gets the item with index i from the list
     *
     * @param i the index to get
     * @return the element to get
     */
    @Override
    public T get(int i) {
        if (!loaded) {
            load();
        }
        return super.get(i);
    }

    /**
     * Get the size of the list
     *
     * @return size of the list
     */
    @Override
    public int size() {
        if (!loaded) {
            load();
        }
        return super.size();
    }

    /**
     * Return the string representation of a lazylist
     * must return an empty list to avoid endless recursion
     *
     * @return [] empty list string
     */
    @Override
    public String toString() {
        if (!loaded) {
            load();
        }

        return String.valueOf(List.of());
    }

    /**
     * Get the hashcode of the lazylist
     * must return 0 to avoid endless recursion
     *
     * @return hashCode
     */
    @Override
    public int hashCode() {
        if (!loaded) {
            load();
        }
        return 0;
    }

    /**
     * Checks if the object is equal to the lazylist
     * does not check the content of the arraylist to avoid endless recursion
     *
     * @param o the object to compare
     * @return true if the objects are equal
     */
    @Override
    public boolean equals(Object o) {
        if (!loaded) {
            load();
        }

        if (o == null) {
            return false;
        }

        if (o == this) {
            return true;
        }

        return o instanceof List;
    }


}
