package at.oswald.ormapper;


import at.oswald.jpql.JPQLParser;
import at.oswald.sql.DeleteStatement;
import at.oswald.sql.InsertStatement;
import at.oswald.sql.SelectStatement;
import at.oswald.sql.UpdateStatement;

import javax.persistence.EntityExistsException;
import javax.persistence.NoResultException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * Class to interact with the persistence context.
 */
public class EntityManager {
    static EntityManager instance = null;
    private final DBProperties dbProperties;
    public final Map<Class<?>, EntityInfo> entitiesInfo = new HashMap<>();
    private Connection connection;
    public final Cache cache = new Cache();


    /**
     * Get an instance of an entityManager
     *
     * @param dbProperties  the properties used to connect to the DB
     * @param entityClasses the classes of the entities
     * @return an instance of an entityManager
     */
    public static EntityManager getInstance(DBProperties dbProperties, Set<Class<?>> entityClasses) {
        if (instance == null) {
            instance = new EntityManager(dbProperties, entityClasses);
        }
        return instance;
    }

    /**
     * Constructs an entityManager with the specified DB and the specified entities
     *
     * @param dbProperties  the properties of the DB
     * @param entityClasses the classes of the entities to manage
     */
    private EntityManager(DBProperties dbProperties, Set<Class<?>> entityClasses) {
        this.dbProperties = dbProperties;
        initialize(entityClasses);
    }

    /**
     * Initialize and set-up the database for the entities
     *
     * @param entityClasses the classes of the entities to manage
     */
    public void initialize(Set<Class<?>> entityClasses) {
        for (Class<?> entityClass : entityClasses) {
            EntityInfo entityInfo = EntityInfo.createFromEntityClass(entityClass);
            entitiesInfo.put(entityClass, entityInfo);
        }
        DBInitializer.initialize(dbProperties, entitiesInfo);

    }

    /**
     * Make an instance managed and persistent.
     *
     * @param entity entity instance
     * @throws EntityExistsException    if the entity already exists.
     *                                  (If the entity already exists, the <code>EntityExistsException</code> may
     *                                  be thrown when the persist operation is invoked, or the
     *                                  <code>EntityExistsException</code> or another <code>PersistenceException</code> may be
     *                                  thrown at flush or commit time.)
     * @throws IllegalArgumentException if the instance is not an entity
     */
    public void persist(Object entity) {
        EntityInfo entityInfo = entitiesInfo.get(entity.getClass());
        if (entityInfo == null) {
            throw new IllegalArgumentException("the instance is not an entity");
        }

        Object id = insertBasicAndManyToOneMappings(entity);
        entityInfo.getPrimaryKeyMapping().setFieldValue(entity, id);

        insertManyToManyMappings(entity, id);
        insertOneToManyMappings(entity, id);

        cache.addEntity(entity, id);

    }

    /**
     * Merge the state of the given entity into the
     * current persistence context.
     *
     * @param entity entity instance
     * @param <T>    the type of the entity
     * @return the managed instance that the state was merged to
     * @throws IllegalArgumentException if instance is not an
     *                                  entity or is a removed entity
     */
    public <T> T merge(T entity) {
        EntityInfo entityInfo = entitiesInfo.get(entity.getClass());
        if (entityInfo == null) {
            throw new IllegalArgumentException("the instance is not an entity");
        }

        try {
            persist(entity);
        } catch (EntityExistsException e) {
            updateEntity(entity);
        }
        cache.addEntity(entity, entityInfo.getIdValue(entity));
        return entity;

    }

    /**
     * Find by primary key.
     * Search for an entity of the specified class and primary key.
     * If the entity instance is contained in the persistence context,
     * it is returned from there.
     *
     * @param entityClass entity class
     * @param primaryKey  primary key
     * @param <T>         the type of the entity
     * @return the found entity instance or null if the entity does
     * not exist
     * @throws IllegalArgumentException if the first argument does
     *                                  not denote an entity type or the primary key is null
     */
    public <T> T find(Class<T> entityClass, Object primaryKey) {
        EntityInfo entityInfo = entitiesInfo.get(entityClass);
        if (!entitiesInfo.containsKey(entityClass)) {
            throw new IllegalArgumentException("the instance is not an entity");
        }
        if (primaryKey == null) {
            throw new IllegalArgumentException("primaryKey should not be null");
        }

        if (cache.containsEntity(entityClass, primaryKey)) {
            return cache.getEntity(entityClass, primaryKey);
        }

        String tableName = entityInfo.getTableName();
        String idFieldName = entityInfo.getPrimaryKeyMapping().getFieldName();
        SelectStatement selectStatement = SelectStatement.forTable(tableName)
                .addEqualsToWhereClause(idFieldName, 1);

        Query<?> query = createNativeQuery(selectStatement.getSQL(), entityClass);
        query.setParameter(1, primaryKey);

        try {
            Object result = query.getSingleResult();
            cache.addEntity(result, entityInfo.getIdValue(result));
            return entityClass.cast(result);
        } catch (NoResultException e) {
            return null;
        }
    }

    /**
     * Remove the entity instance.
     *
     * @param entity entity instance
     * @throws IllegalArgumentException if the instance is not an entity
     */
    public void remove(Object entity) {
        Class<?> entityClass = entity.getClass();
        EntityInfo entityInfo = entitiesInfo.get(entityClass);
        if (!entitiesInfo.containsKey(entityClass)) {
            throw new IllegalArgumentException("the instance is not an entity");
        }

        String idFieldName = entityInfo.getPrimaryKeyMapping().getFieldName();
        Object id = entityInfo.getIdValue(entity);

        entityInfo.getManyToManyMappings()
                .forEach(mappingManyToMany -> mappingManyToMany.deleteFromAssociativeTable(id, this));

        // Try to set related OneToMany entries to null
        entityInfo.getOneToManyMappings()
                .forEach(mappingInfo -> mappingInfo.setRelatedEntriesToNull(id, this));

        // Delete table entries for this entity
        String tableName = entityInfo.getTableName();
        DeleteStatement deleteStatement = DeleteStatement
                .forTable(tableName)
                .addEqualsToWhereClause(idFieldName, 1);
        Query<?> deleteQuery = createNativeQuery(deleteStatement.getSQL(), null);
        deleteQuery.setParameter(1, id);
        deleteQuery.executeUpdate();

        if (id != null && cache.containsEntity(entityClass, id)) {
            cache.removeEntity(entity, id);
        }

    }

    /**
     * Create an instance of <code>Query</code> for executing
     * a native SQL query.
     *
     * @param sqlString   a native SQL query string
     * @param resultClass the class of the resulting instance(s)
     * @param <T>         the type of the entity
     * @return the new query instance
     */
    public <T> Query<T> createNativeQuery(String sqlString, Class<T> resultClass) {
        return new Query<>(this, sqlString, resultClass);
    }

    /**
     * Create an instance of <code>Query</code> for executing a
     * Jakarta Persistence query language statement.
     *
     * @param qlString a Jakarta Persistence query string
     * @return the new query instance
     * @throws IllegalArgumentException if the query string is
     *                                  found to be invalid
     */
    public <T> Query<T> createQuery(String qlString, Class<T> resultClass) {
        JPQLParser parser = new JPQLParser(qlString, entitiesInfo);
        var nativeSQL = parser.getNativeSQL();

        return createNativeQuery(nativeSQL, resultClass);
    }

    /**
     * Return the resource-level <code>EntityTransaction</code> object.
     * The <code>EntityTransaction</code> instance may be used serially to
     * begin and commit multiple transactions.
     *
     * @return EntityTransaction instance
     * @throws IllegalStateException if invoked on a JTA
     *                               entity manager
     */
    public EntityTransaction getTransaction() {
        return new EntityTransaction(this);
    }

    /**
     * Inserts the basic and m:1 mappings of an entity into the DB
     *
     * @param entity the entity
     * @return the id of the entity
     */
    private Object insertBasicAndManyToOneMappings(Object entity) {
        EntityInfo entityInfo = entitiesInfo.get(entity.getClass());
        List<MappingInfo> basicAndManyToOneMappings = entityInfo.getBasicAndManyToOneMappings();
        List<ColumnInfo> columnInfos = basicAndManyToOneMappings.stream()
                .map(MappingInfo::getColumnInfoEntity)
                .collect(Collectors.toList());
        InsertStatement insertStatement = new InsertStatement(entityInfo.getTableName(), columnInfos);
        Query<?> query = createNativeQuery(insertStatement.getSQL(), null);

        for (var mappingInfo : basicAndManyToOneMappings) {
            Object fieldValue = mappingInfo.getFieldValue(entity, entitiesInfo.get(mappingInfo.getRelatedEntityClass()));
            query.setParameter(mappingInfo.getColumnInfoEntity().getColumnName(), fieldValue);
        }

        return query.executeInsert();
    }


    /**
     * Inserts the m:m mappings of an entity into the DB
     *
     * @param entity   the entity
     * @param entityId the id of the entity
     */
    private void insertManyToManyMappings(Object entity, Object entityId) {
        EntityInfo entityInfo = entitiesInfo.get(entity.getClass());
        List<MappingManyToMany> manyToManyMappings = entityInfo.getManyToManyMappings();

        manyToManyMappings.forEach(mappingInfo -> mappingInfo.insert(entity, entityId, this));
    }

    /**
     * Inserts the 1:m mappings of an entity into the DB
     *
     * @param entity   the entity
     * @param entityId the id of the entity
     */
    private void insertOneToManyMappings(Object entity, Object entityId) {
        EntityInfo entityInfo = entitiesInfo.get(entity.getClass());
        List<MappingOneToMany> oneToManyMappings = entityInfo.getOneToManyMappings();

        oneToManyMappings.forEach(mappingInfo -> mappingInfo.updateForeignKeysInRelatedTable(entity, entityId, this));
    }

    /**
     * Update an existing entity
     *
     * @param entity the entity with the values to update
     */
    private void updateEntity(Object entity) {
        EntityInfo entityInfo = entitiesInfo.get(entity.getClass());
        UpdateStatement statement = new UpdateStatement(
                entityInfo.getTableName(),
                entityInfo.getPrimaryKeyColumn(),
                entityInfo.getNonPrimaryKeyColumns());
        Query<?> query = createNativeQuery(statement.getSQL(), entity.getClass());

        for (MappingInfo mappingInfo : entityInfo.getBasicAndManyToOneMappings()) {
            Object fieldValue = mappingInfo.getFieldValue(entity, entitiesInfo.get(mappingInfo.getRelatedEntityClass()));
            query.setParameter(mappingInfo.getColumnInfoEntity().getColumnName(), fieldValue);
        }
        query.executeUpdate();

        entityInfo.getOneToManyMappings().forEach(
                mappingOneToMany -> mappingOneToMany.updateRelatedTable(entity, entityInfo, this)
        );
        entityInfo.getManyToManyMappings().forEach(
                mappingManyToMany -> mappingManyToMany.updateAssociativeTable(entity, entityInfo, this)
        );

    }

    /**
     * Returns if the entityManager has an active connection opened
     *
     * @return if the entityManager has an active connection opened
     */
    boolean isConnectionActive() {
        return connection != null;
    }

    /**
     * Get a db connection, opens a new one if there was no open connection before
     *
     * @return a db connection
     */
    Connection getConnection() {
        if (connection == null) {
            try {
                connection = DriverManager.getConnection(dbProperties.getURLWithDatabase(), dbProperties.getUsername(), dbProperties.getPassword());
                connection.setAutoCommit(false);
            } catch (SQLException e) {
                SQLExceptionHandler.handle(e);
            }
        }
        return connection;
    }

    /**
     * Close the db connection
     */
    void closeConnection() {
        try {
            if (connection != null) {
                connection.close();
                connection = null;
            }
        } catch (SQLException e) {
            SQLExceptionHandler.handle(e);
        }
    }

    /**
     * Close the entityManager
     */
    public void close() {
        closeConnection();
    }

}
