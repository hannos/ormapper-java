package at.oswald.ormapper;


import java.util.HashMap;
import java.util.Map;


/**
 * Cache for entities of all classes
 */
public class Cache {
    Map<Class<?>, Map<Object, Object>> cachedEntities = new HashMap<>();

    /**
     * Checks if the cache contains a entity with this class and this primaryKey
     *
     * @param entityClass the class of the entity to find
     * @param primaryKey  the primaryKey of the entity to find
     * @return true if the cache contains a entity with this class and primaryKey
     */
    public boolean containsEntity(Class<?> entityClass, Object primaryKey) {
        Map<Object, Object> cachedEntitiesWithEntityClass = cachedEntities.get(entityClass);

        if (cachedEntitiesWithEntityClass != null) {
            return cachedEntitiesWithEntityClass.containsKey(primaryKey);
        }
        return false;
    }

    /**
     * Adds an entity to the cache
     *
     * @param entity     the entity to cache
     * @param primaryKey the primaryKey of the entity
     */
    public void addEntity(Object entity, Object primaryKey) {
        cachedEntities.put(entity.getClass(),
                new HashMap<>(Map.of(primaryKey, entity))
        );
    }

    /**
     * Removes the entity from the cache
     *
     * @param entity     the entity to remove
     * @param primaryKey the primaryKey of the entity to remove
     */
    public void removeEntity(Object entity, Object primaryKey) {
        Map<Object, Object> cachedEntitiesWithEntityClass = cachedEntities.get(entity.getClass());

        if (cachedEntitiesWithEntityClass != null) {
            cachedEntitiesWithEntityClass.remove(primaryKey);
        }
    }

    /**
     * Clears the cache for this entity class
     *
     * @param entityClass the class of entities to remove
     */
    public void clearCacheForEntityClass(Class<?> entityClass) {
        Map<Object, Object> mapForEntityClass = cachedEntities.get(entityClass);

        if (mapForEntityClass != null) {
            mapForEntityClass.clear();
        }

    }

    /**
     * Gets an entity from the cache
     *
     * @param entityClass the class of the entity to get
     * @param primaryKey  the primaryKey of the entity to get
     * @param <T>         the type of the entity
     * @return entity from the cache
     */
    public <T> T getEntity(Class<T> entityClass, Object primaryKey) {
        Map<Object, Object> cachedEntitiesWithEntityClass = cachedEntities.get(entityClass);

        if (cachedEntitiesWithEntityClass != null) {
            return entityClass.cast(cachedEntitiesWithEntityClass.get(primaryKey));
        }

        throw new IllegalArgumentException("Cache does not contain this entity! Check before trying to get entity from cache!");

    }

}
