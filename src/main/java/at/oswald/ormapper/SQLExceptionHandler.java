package at.oswald.ormapper;

import javax.persistence.EntityExistsException;
import javax.persistence.PersistenceException;
import java.sql.SQLException;

/**
 * Class to handle SQL Exceptions
 */
public class SQLExceptionHandler {


    /**
     * Handle SQL exceptions
     *
     * @param exception the exception to handle
     */
    public static void handle(SQLException exception) {
        if (exception.getErrorCode() == 1062) {
            throw new EntityExistsException(exception);
        } else if (exception.getErrorCode() == 1451) {
            throw new PersistenceException("Cannot delete or update a parent row: a foreign key constraint fails");
        } else {
            System.out.println("SQL Exception: errorCode: " + exception.getErrorCode());
            exception.printStackTrace();
        }

    }
}
