package at.oswald.ormapper;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Class for storing DB properties
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DBProperties {
    private String url;
    private String dbName;
    private String username;
    private String password;

    /**
     * Returns the URL with the database/schema selected
     *
     * @return URL with the database/schema selected
     */
    public String getURLWithDatabase() {
        return url + "/" + dbName;
    }
}
