package at.oswald.ormapper;


import java.sql.ResultSet;

/**
 * Contains the information for a mapping of a field to a DB representation
 */
public interface MappingInfo {

    /**
     * Get field name of this mapping
     *
     * @return name of the field for this mapping
     */
    String getFieldName();

    /**
     * Return information of the column for this mapping
     *
     * @return information of the column for this mapping
     */
    ColumnInfo getColumnInfoEntity();

    /**
     * Return information about the column for this mapping in a related table
     *
     * @return information about the column for this mapping in a related table
     */
    ColumnInfo getColumnInfoRelatedEntity();

    /**
     * Return the class of the entity which has this mapping
     *
     * @return the class of the entity which has this mapping
     */
    Class<?> getEntityClass();

    /**
     * Return the class of the related entity which has a relation to this mapping
     *
     * @return the class of the related entity which has a relation to this mapping
     */
    Class<?> getRelatedEntityClass();

    /**
     * Get the value of a entity with this mapping
     *
     * @param entity            the entity with the value
     * @param relatedEntityInfo information about the related entity
     * @return the value to get
     */
    default Object getFieldValue(Object entity, EntityInfo relatedEntityInfo) {
        return ReflectionUtility.getFieldValueFromObject(entity, getFieldName());
    }

    /**
     * Sets the field value of a entity with this mapping
     *
     * @param entity the entity where the field value should be set
     * @param value  the value to set
     */
    default void setFieldValue(Object entity, Object value) {
        ReflectionUtility.setFieldValueInObject(entity, getFieldName(), value);
    }

    /**
     * Get the value for this mapping from a result set
     *
     * @param resultSet     the result set
     * @param entityManager a entity manager instance
     * @param entityInfo    the entityInfo for the entity, which has this mapping
     * @return the field value for this mapping
     */
    Object getFieldValueFromResultSet(ResultSet resultSet, EntityManager entityManager, EntityInfo entityInfo);
}
