package at.oswald.ormapper;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Parameter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Class for select,update and delete queries
 *
 * @param <T> the type of the result
 */
public class Query<T> {
    final EntityManager entityManager;
    final Map<ParameterImpl<?>, Object> parameterValueMap = new HashMap<>();
    final Class<T> resultClass;
    final List<ParameterImpl<?>> jdbcParameterInOrder = new ArrayList<>();
    final String positionalParameterRegex = "\\?[1-9]+";
    final String namedParameterRegex = ":[0-9A-z]+";
    private static final Boolean PRINT_SQL = false;
    String sqlString;

    /**
     * Constructs a query
     *
     * @param entityManager a entityManager instance
     * @param sqlString     a native sql string
     * @param resultClass   the class of the result
     */
    Query(EntityManager entityManager, String sqlString, Class<T> resultClass) {
        this.entityManager = entityManager;
        this.sqlString = sqlString;
        this.resultClass = resultClass;

        parsePositionalParameters();
        replacePositionalParameters();

        parseNamedParameters();
        replaceNamedParameters();
    }

    /**
     * Bind an argument value to a positional parameter.
     *
     * @param position position
     * @param value    parameter value
     * @return the same query instance
     * @throws IllegalArgumentException if position does not correspond to a positional parameter of the query
     */
    public Query<T> setParameter(int position, Object value) {
        for (var p : parameterValueMap.keySet()) {
            if (p.getPosition() == position) {
                parameterValueMap.put(p, value);
                return this;
            }
        }
        throw new IllegalArgumentException("position does not correspond to a positional parameter of the query");
    }


    /**
     * Bind an argument value to a named parameter.
     *
     * @param name  name of the parameter
     * @param value parameter value
     * @return the same query instance
     */
    public Query<T> setParameter(String name, Object value) {
        for (var p : parameterValueMap.keySet()) {
            if (p.getName().equals(name)) {
                parameterValueMap.put(p, value);
                return this;
            }
        }
        throw new IllegalArgumentException("name does not correspond to a named parameter of the query");
    }

    /**
     * Get the parameter objects corresponding to the declared parameters of the query.
     *
     * @return set of the parameter objects
     */
    public Set<Parameter<?>> getParameters() {
        return Set.copyOf(jdbcParameterInOrder);
    }

    /**
     * Executes a query and returns a list of maps with the columnNames and their values
     *
     * @param columnInfos the columns for this query
     * @return list of maps with the columnNames and their values
     */
    public List<Map<String, Object>> getValuesForColumns(List<ColumnInfo> columnInfos) {
        try (PreparedStatement statement = entityManager.getConnection().prepareStatement(sqlString)) {
            List<Map<String, Object>> results = new ArrayList<>();

            if (!sqlString.toUpperCase().startsWith("SELECT")) {
                throw new IllegalStateException("getKeyValuesFromDB only supports SELECT statements");
            }

            for (int i = 0; i < jdbcParameterInOrder.size(); i++) {
                statement.setObject(i + 1, parameterValueMap.get(jdbcParameterInOrder.get(i)));
            }
            if (PRINT_SQL) {
                System.out.println(sqlString);
            }
            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                Map<String, Object> map = new HashMap<>();

                for (ColumnInfo columnInfo : columnInfos) {
                    String columnName = columnInfo.getColumnName();
                    var result = rs.getObject(columnName);
                    map.put(columnName, result);
                }
                results.add(map);
            }
            rs.close();
            statement.close();
            return results;
        } catch (SQLException e) {
            SQLExceptionHandler.handle(e);
        }
        return null;
    }

    /**
     * Execute a SELECT query and return the query results as a typed List.
     *
     * @return a list of the results
     * @throws IllegalStateException if called for an other statement than select
     */
    public List<T> getResultList() {
        try (PreparedStatement statement = entityManager.getConnection().prepareStatement(sqlString)) {
            List<T> results = new ArrayList<>();

            if (!sqlString.toUpperCase().startsWith("SELECT")) {
                throw new IllegalStateException("getResultList only supports SELECT statements");
            }

            for (int i = 0; i < jdbcParameterInOrder.size(); i++) {
                statement.setObject(i + 1, parameterValueMap.get(jdbcParameterInOrder.get(i)));
            }
            if (PRINT_SQL) {
                System.out.println(sqlString);
            }
            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                T result = createEntityFromResultSet(resultClass, rs);
                results.add(result);

            }
            rs.close();
            statement.close();
            return results;

        } catch (SQLException e) {
            SQLExceptionHandler.handle(e);
        }
        return null;
    }

    /**
     * Execute a SELECT query that returns a single result.
     *
     * @return the result
     * @throws NoResultException        if there is no result
     * @throws NonUniqueResultException if more than one result
     * @throws IllegalStateException    if called for an other statement than select
     */
    public T getSingleResult() {
        T result = null;
        try (PreparedStatement statement = entityManager.getConnection().prepareStatement(sqlString)) {

            if (!sqlString.toUpperCase().startsWith("SELECT")) {
                throw new IllegalStateException("getSingleResult only supports SELECT statements");
            }

            for (int i = 0; i < jdbcParameterInOrder.size(); i++) {
                statement.setObject(i + 1, parameterValueMap.get(jdbcParameterInOrder.get(i)));
            }
            if (PRINT_SQL) {
                System.out.println(sqlString);
            }
            ResultSet rs = statement.executeQuery();


            if (rs.next()) {
                result = createEntityFromResultSet(resultClass, rs);
            } else {
                throw new NoResultException();
            }
            if (rs.next()) {
                throw new NonUniqueResultException();
            }
            rs.close();

        } catch (SQLException e) {
            SQLExceptionHandler.handle(e);
        }
        return result;
    }

    /**
     * Execute a UPDATE or DELETE query and returns the affected row count
     *
     * @return affected row count
     * @throws IllegalStateException if called for an other statement than update or delete
     */
    public int executeUpdate() {

        if (!sqlString.toLowerCase().startsWith("update") && !sqlString.toLowerCase().startsWith("delete")) {
            throw new IllegalStateException("executeUpdate only supports UPDATE and DELETE statements!");
        }

        try (PreparedStatement statement = entityManager.getConnection().prepareStatement(sqlString)) {

            for (int i = 0; i < jdbcParameterInOrder.size(); i++) {
                statement.setObject(i + 1, parameterValueMap.get(jdbcParameterInOrder.get(i)));
            }
            if (PRINT_SQL) {
                System.out.println(sqlString);
            }
            int row_count = statement.executeUpdate();

            for (Class<?> entityClass : getAffectedEntities(sqlString)) {
                entityManager.cache.clearCacheForEntityClass(entityClass);
            }

            return row_count;


        } catch (SQLException e) {
            SQLExceptionHandler.handle(e);
        }
        return -1;
    }

    /**
     * Execute a INSERT query and return the new id
     *
     * @return id of the inserted entity
     * @throws IllegalStateException if called for an other statement than insert
     */
    public Object executeInsert() {
        Object id = null;

        if (!sqlString.toLowerCase().startsWith("insert")) {
            throw new IllegalStateException("ExecuteInsert only supports insert statements!");
        }

        try (PreparedStatement statement = entityManager.getConnection().prepareStatement(sqlString, Statement.RETURN_GENERATED_KEYS)) {

            for (int i = 0; i < jdbcParameterInOrder.size(); i++) {
                statement.setObject(i + 1, parameterValueMap.get(jdbcParameterInOrder.get(i)));
            }
            if (PRINT_SQL) {
                System.out.println(sqlString);
            }
            statement.executeUpdate();

            ResultSet rs = statement.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getObject(1);
            }

            Set<Class<?>> affectedEntities = getAffectedEntities(sqlString);
            for (Class<?> entityClass : affectedEntities) {
                entityManager.cache.clearCacheForEntityClass(entityClass);

                EntityInfo entityInfo = entityManager.entitiesInfo.get(entityClass);
                entityInfo.getManyToOneMappings()
                        .forEach(mappingInfo -> entityManager.cache.clearCacheForEntityClass(mappingInfo.getRelatedEntityClass()));
            }

        } catch (SQLException e) {
            SQLExceptionHandler.handle(e);
        }
        return id;
    }

    /**
     * Create an entity from a result set
     *
     * @param entityClass the class of the entity to create
     * @param rs          the result set
     * @return the new entity
     */
    private T createEntityFromResultSet(Class<T> entityClass, ResultSet rs) {
        try {
            final T entity = entityClass.getDeclaredConstructor().newInstance();
            EntityInfo entityInfo = entityManager.entitiesInfo.get(entityClass);
            entityInfo.getMappingInfos().forEach(
                    (mappingInfo) -> {
                        Object fieldValue = mappingInfo.getFieldValueFromResultSet(rs, entityManager, entityInfo);
                        mappingInfo.setFieldValue(entity, fieldValue);
                    }
            );
            return entity;
        } catch (ReflectiveOperationException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse the positional parameters in the query
     */
    private void parsePositionalParameters() {
        Matcher matcher = Pattern.compile(positionalParameterRegex).matcher(sqlString);
        while (matcher.find()) {
            String placeholder = matcher.group();
            ParameterImpl<?> parameter = new ParameterImpl<>(null, Integer.valueOf(placeholder.substring(1)), null);
            parameterValueMap.put(parameter, null);
            jdbcParameterInOrder.add(parameter);
        }
    }

    /**
     * Remove the number from the positional parameters
     */
    private void replacePositionalParameters() {
        sqlString = sqlString.replaceAll(positionalParameterRegex, "?");
    }

    /**
     * Parse the named parameters in the query
     */
    private void parseNamedParameters() {
        Matcher matcher = Pattern.compile(namedParameterRegex).matcher(sqlString);
        while (matcher.find()) {
            String placeholder = matcher.group();
            ParameterImpl<?> parameter = new ParameterImpl<>(placeholder.substring(1), null, null);
            parameterValueMap.put(parameter, null);
            jdbcParameterInOrder.add(parameter);
        }
    }

    /**
     * Replace named parameters with ?
     */
    private void replaceNamedParameters() {
        sqlString = sqlString.replaceAll(namedParameterRegex, "?");
    }

    /**
     * Get the class of the entities which were affected by the sql statement
     *
     * @param sql the sql statement
     * @return set of affected entities
     */
    private Set<Class<?>> getAffectedEntities(String sql) {
        String sqlLowerCase = sql.toLowerCase();
        Set<Class<?>> result = new HashSet<>();

        entityManager.entitiesInfo.forEach((aClass, entityInfo) -> {
            if (sqlLowerCase.contains(entityInfo.getTableName())) {
                result.add(aClass);
            }
        });

        return result;
    }

}
