package at.oswald.ormapper;


/**
 * Non caching cache implementation for testing
 */
public class NullCache extends Cache {

    @Override
    public boolean containsEntity(Class<?> entityClass, Object primaryKey) {
        return false;
    }

    @Override
    public void addEntity(Object entity, Object primaryKey) {
    }

    @Override
    public void removeEntity(Object entity, Object primaryKey) {
    }

    @Override
    public void clearCacheForEntityClass(Class<?> entityClass) {
    }

    @Override
    public <T> T getEntity(Class<T> entityClass, Object primaryKey) {
        throw new IllegalArgumentException("Cache does not contain this entity! Check before trying to get entity from cache!");
    }
}
