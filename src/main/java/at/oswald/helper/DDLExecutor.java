package at.oswald.helper;

import at.oswald.ormapper.ColumnInfo;
import at.oswald.ormapper.DBProperties;
import at.oswald.ormapper.SQLExceptionHandler;
import at.oswald.sql.DDLStatement;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;


/**
 * Helper Class to create and execute DDL Statements
 */
public class DDLExecutor {
    private final DBProperties dbProperties;
    private static final boolean PRINT_SQL = false;


    /**
     * Constructs a DDL executor for the specified DB
     *
     * @param dbProperties the properties of the DB
     */
    public DDLExecutor(DBProperties dbProperties) {
        this.dbProperties = dbProperties;
    }


    /**
     * Creates a CreateDatabaseIfNotExistsStatement SQL
     *
     * @param databaseName the name of the database
     * @return SQL String
     */
    public static String buildCreateDatabaseIfNotExistsStatement(String databaseName) {
        return String.format("CREATE DATABASE IF NOT EXISTS %s;", databaseName);
    }

    /**
     * Creates a DropDatabaseIfExistsStatement SQL
     *
     * @param databaseName the name of the database
     * @return SQL String
     */
    public static String buildDropDatabaseIfExistsStatement(String databaseName) {
        return String.format("DROP DATABASE IF EXISTS %s;", databaseName);
    }

    /**
     * Creates the database if it does not exist, otherwise does nothing
     */
    public void createDatabaseIfNotExists() {
        System.out.println("Creating database " + dbProperties.getDbName());
        String sql = buildCreateDatabaseIfNotExistsStatement(dbProperties.getDbName());

        executeDDLSQL(sql, false);
        System.out.println("Creating successfull");
    }

    /**
     * Drops the database if it does not exist, otherwise does nothing
     */
    public void dropDatabaseIfExists() {
        System.out.println("Dropping database " + dbProperties.getDbName());
        String sql = buildDropDatabaseIfExistsStatement(dbProperties.getDbName());

        executeDDLSQL(sql, false);
        System.out.println("Dropping successfull");

    }

    /**
     * Creates the database a table if it does not exist, otherwise does nothing
     *
     * @param tableName      the name of the new table
     * @param columnInfoList the colums of the new table to create
     */
    public void createTableIfNotExists(String tableName, List<ColumnInfo> columnInfoList) {
        String sql = DDLStatement.buildCreateTableIfNotExitsStatement(tableName, columnInfoList);

        executeDDLSQL(sql, true);
    }

    /**
     * Executes a DDL SQL, throws an exception if the SQL is not a DDL Statement
     *
     * @param sql the SQL to execute
     */
    public void executeDDLSQL(String sql) {
        String sqlLC = sql.toLowerCase();

        if (!sqlLC.startsWith("create") && !sqlLC.startsWith("alter")) {
            throw new IllegalArgumentException("Only DDL statements supported!");
        }

        executeDDLSQL(sql, true);
    }

    /**
     * Executes a DDL SQL
     *
     * @param sqlString      the SQL to execute
     * @param withDBSelected flag to indicate that the statement should be executed for a database(schema)
     */
    private void executeDDLSQL(String sqlString, boolean withDBSelected) {
        String url = withDBSelected ? dbProperties.getURLWithDatabase() : dbProperties.getUrl();
        DriverManager.setLoginTimeout(1);

        try (Connection connection = DriverManager.getConnection(url, dbProperties.getUsername(), dbProperties.getPassword());
             Statement stmt = connection.createStatement()) {

            if (PRINT_SQL) {
                System.out.println(sqlString);
            }
            stmt.executeUpdate(sqlString);
        } catch (SQLException e) {
            SQLExceptionHandler.handle(e);
        }
    }
}
