package at.oswald.helper;

import at.oswald.ormapper.DBProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.File;
import java.io.IOException;

public class HelperClass {
    private static final String TEST_PROPERTIES_PATH = "src/main/resources/database.yml";

    public static DBProperties getDBPropertiesForTesting() {
        try {
            return new ObjectMapper(new YAMLFactory()).readValue(new File(TEST_PROPERTIES_PATH), DBProperties.class);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Check if database.yml exists!");
            System.exit(1);
        }
        return null;
    }
}
