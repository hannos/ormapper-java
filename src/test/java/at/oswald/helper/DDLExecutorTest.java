package at.oswald.helper;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class DDLExecutorTest {

    @Test
    void buildCreateDatabaseIfNotExistsStatement() {
        var databaseName = "myDB";
        var SQLStatement = DDLExecutor.buildCreateDatabaseIfNotExistsStatement(databaseName);

        assertTrue(SQLStatement.contains(String.format("CREATE DATABASE IF NOT EXISTS %s;", databaseName)));
    }

    @Test
    void buildDropDatabaseIfExistsStatement() {
        var databaseName = "myDB";
        var SQLStatement = DDLExecutor.buildDropDatabaseIfExistsStatement(databaseName);

        assertTrue(SQLStatement.contains(String.format("DROP DATABASE IF EXISTS %s;", databaseName)));
    }
}