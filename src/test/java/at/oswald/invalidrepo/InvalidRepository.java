package at.oswald.invalidrepo;

import at.oswald.repository.BaseJpaRepository;

//ID Type doesnt not match the type in the entity
public interface InvalidRepository extends BaseJpaRepository<InvalidEntity, Long> {
}