package at.oswald.invalidrepo;

import at.oswald.repository.RepositoryInterfaceInspector;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class RepositoryInterfaceInspectorTest {


    @Test
    void repository_with_unfitting_id_type_should_throw_exception() {
        var inspector = new RepositoryInterfaceInspector(InvalidRepository.class);
        assertThrows(IllegalArgumentException.class, inspector::check);
    }

    @Test
    void repository_with_unfitting_id_type_should_throw_exception2() {
        var inspector = new RepositoryInterfaceInspector(InvalidRepository2.class);
        assertThrows(IllegalArgumentException.class, inspector::check);
    }


}