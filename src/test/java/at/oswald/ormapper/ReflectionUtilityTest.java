package at.oswald.ormapper;

import at.example.application.entity.Author;
import org.junit.jupiter.api.Test;

import javax.persistence.OneToMany;
import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ReflectionUtilityTest {


    @Test
    void test_findFieldNameWithAnnotation() throws NoSuchFieldException {
        Class<?> authorClass = Author.class;

        Field fieldWithOneToMany = ReflectionUtility.findFieldWithAnnotation(authorClass, OneToMany.class);

        assertEquals(authorClass.getDeclaredField("newsList"), fieldWithOneToMany);

    }
}