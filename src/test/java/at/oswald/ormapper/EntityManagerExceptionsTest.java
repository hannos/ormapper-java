package at.oswald.ormapper;

import at.example.application.entity.Author;
import at.example.application.entity.Category;
import at.example.application.entity.News;
import at.oswald.helper.DDLExecutor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityExistsException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Set;

import static at.oswald.helper.HelperClass.getDBPropertiesForTesting;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class EntityManagerExceptionsTest {
    static DBProperties testDBProperties = getDBPropertiesForTesting();
    private static final DDLExecutor ddlExecutor = new DDLExecutor(testDBProperties);
    private final Set<Class<?>> singleton = Set.of(Author.class, News.class, Category.class);
    EntityManager entityManager;


    @BeforeAll
    static void setUpAll() {
        ddlExecutor.dropDatabaseIfExists();
        ddlExecutor.createDatabaseIfNotExists();
    }

    @BeforeEach
    void setUpEach() {
        entityManager = EntityManager.getInstance(testDBProperties, singleton);
        entityManager.initialize(singleton);
        entityManager.getTransaction().begin();
    }

    @AfterEach
    void tearDown() {
        entityManager.getTransaction().rollback();
        entityManager.close();
    }

    @Test
    void persisting_an_existing_entity_should_throw_an_exception() {
        News entity = new News(1L, "title", "abc", true, Timestamp.valueOf(LocalDateTime.of(2020, 12, 12, 12, 5)), null, null);

        entityManager.persist(entity);
        assertThrows(EntityExistsException.class, () -> entityManager.persist(entity));
    }

    @Test
    void persisting_an_non_entity_should_throw_an_exception() {
        assertThrows(IllegalArgumentException.class, () -> entityManager.persist(new StringBuilder("wef")));
    }

    @Test
    void merging_an_non_entity_should_throw_an_exception() {
        assertThrows(IllegalArgumentException.class, () -> entityManager.merge(new StringBuilder("wef")));
    }
}
