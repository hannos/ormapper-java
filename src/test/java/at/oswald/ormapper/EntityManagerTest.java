package at.oswald.ormapper;


import at.example.application.entity.Author;
import at.example.application.entity.Category;
import at.example.application.entity.News;
import at.oswald.helper.DDLExecutor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

import static at.oswald.helper.HelperClass.getDBPropertiesForTesting;
import static org.junit.jupiter.api.Assertions.*;

class EntityManagerTest {
    static DBProperties testDBProperties = getDBPropertiesForTesting();
    private static final DDLExecutor ddlExecutor = new DDLExecutor(testDBProperties);
    private final Set<Class<?>> singleton = Set.of(Author.class, News.class, Category.class);
    EntityManager entityManager;


    @BeforeAll
    static void setUpAll() {
        ddlExecutor.dropDatabaseIfExists();
        ddlExecutor.createDatabaseIfNotExists();
    }

    @BeforeEach
    void setUpEach() {
        entityManager = EntityManager.getInstance(testDBProperties, singleton);
        entityManager.initialize(singleton);
        entityManager.getTransaction().begin();
    }

    @AfterEach
    void tearDown() {
        if (entityManager.getTransaction().isActive()) {
            entityManager.getTransaction().rollback();
        }
        entityManager.close();
    }

    @Test
    void remove_existing_entity_should_succeed() {
        News news = new News("title", "abc", true, Timestamp.valueOf(LocalDateTime.of(2020, 12, 12, 12, 5)), null, null);


        entityManager.persist(news);
        Query<?> query = entityManager.createNativeQuery("SELECT * FROM news", News.class);
        assertEquals(1, query.getResultList().size());


        entityManager.remove(news);
        assertEquals(0, query.getResultList().size());
    }

    @Test
    void remove_non_entity_should_throw_exception() {
        String testString = "test";
        assertThrows(IllegalArgumentException.class, () -> entityManager.remove(testString));
    }

    @Test
    void merge_changes() {
        News news = new News("title", "abc", true, Timestamp.valueOf(LocalDateTime.of(2020, 12, 12, 12, 5)), null, List.of());
        entityManager.persist(news);

        assertEquals("title", news.getTitle());
        News testEntityFromDB = entityManager.find(News.class, news.getId());

        assertEquals(news, testEntityFromDB);

        testEntityFromDB.setTitle("otherString");

        entityManager.merge(testEntityFromDB);

        testEntityFromDB = entityManager.find(News.class, news.getId());
        assertEquals("otherString", testEntityFromDB.getTitle());

    }

    @Test
    void merge_change_1_to_n() {
        Author author1 = new Author("first", "author");
        Author author2 = new Author("second", "author");
        News news = new News("Title", "text", false, Timestamp.from(Instant.now()), author1);
        entityManager.persist(author1);
        entityManager.persist(author2);
        entityManager.persist(news);

        News newsFromDB = entityManager.find(News.class, news.getId());
        assertEquals("first", newsFromDB.getAuthor().getFirstName());

        news.setAuthor(author2);
        entityManager.merge(news);

        newsFromDB = entityManager.find(News.class, news.getId());
        assertEquals("second", newsFromDB.getAuthor().getFirstName());

    }

    @Test
    void merge_change_1_to_n_null() {
        Author author1 = new Author("first", "author");
        Author author2 = new Author("second", "author");
        News news = new News("Title", "text", false, Timestamp.from(Instant.now()), author1);
        entityManager.persist(author1);
        entityManager.persist(author2);
        entityManager.persist(news);

        News newsFromDB = entityManager.find(News.class, news.getId());
        assertEquals("first", newsFromDB.getAuthor().getFirstName());

        news.setAuthor(null);
        entityManager.merge(news);

        newsFromDB = entityManager.find(News.class, news.getId());
        assertNull(newsFromDB.getAuthor());

    }

    @Test
    void merge_change_n_to_n() {
        News news = new News("Title", "text", false, Timestamp.from(Instant.now()), null, List.of());
        entityManager.persist(news);

        News newsFromDB = entityManager.find(News.class, news.getId());
        assertEquals(0, newsFromDB.getCategories().size());

        Category category1 = new Category("Sport", List.of());
        Category category2 = new Category("Regionales", List.of());
        entityManager.persist(category1);
        entityManager.persist(category2);

        news.setCategories(List.of(category1, category2));
        entityManager.merge(news);

        newsFromDB = entityManager.find(News.class, news.getId());
        assertEquals(2, newsFromDB.getCategories().size());
        assertEquals(List.of(category1, category2), newsFromDB.getCategories());

    }

    @Test
    void merge_change_n_to_n_null() {
        Category category1 = new Category("Sport", List.of());
        Category category2 = new Category("Regionales", List.of());
        News news = new News("Title", "text", false, Timestamp.from(Instant.now()), null, List.of(category1, category2));
        entityManager.persist(category1);
        entityManager.persist(category2);
        entityManager.persist(news);

        News newsFromDB = entityManager.find(News.class, news.getId());
        assertEquals(category1, newsFromDB.getCategories().get(0));
        assertEquals(category2, newsFromDB.getCategories().get(1));

        news.setCategories(List.of());
        entityManager.merge(news);

        newsFromDB = entityManager.find(News.class, news.getId());
        assertEquals(List.of(), newsFromDB.getCategories());

    }

    @Test
    void merge_author() {
        News news1 = new News("Title1", "text1", false, Timestamp.from(Instant.now()), null);
        News news2 = new News("Title2", "text2", false, Timestamp.from(Instant.now()), null);
        entityManager.persist(news1);
        entityManager.persist(news2);
        Author author1 = new Author("first", "author", List.of(news1, news2));
        entityManager.persist(author1);

        assertEquals(2, author1.getNewsList().size());

        Author authorFromDB = entityManager.find(Author.class, author1.getId());
        assertNotNull(authorFromDB);

        assertEquals(2, authorFromDB.getNewsList().size());


        authorFromDB.setFirstName("second");
        authorFromDB.setLastName("lastname");
        authorFromDB.setNewsList(List.of(news1));
        entityManager.merge(authorFromDB);

        authorFromDB = entityManager.find(Author.class, author1.getId());
        assertEquals("second", authorFromDB.getFirstName());
        assertEquals("lastname", authorFromDB.getLastName());
        assertEquals(1, authorFromDB.getNewsList().size());

    }

    @Test
    void test_jpql_query_news() {
        News news1 = new News("Title1", "text1", false, Timestamp.from(Instant.now()), null);
        News news2 = new News("Title2", "text2", false, Timestamp.from(Instant.now()), null);
        News news3 = new News("ABCDE", "text3", false, Timestamp.from(Instant.now()), null);
        entityManager.persist(news1);
        entityManager.persist(news2);
        entityManager.persist(news3);

        assertEquals(3, entityManager.createQuery("select n from News n", News.class).getResultList().size());

        Query<News> titleLikeQuery = entityManager.createQuery("select n from News n where n.title like ?1", News.class);
        titleLikeQuery.setParameter(1, "Title%");

        List<News> newsList = titleLikeQuery.getResultList();

        assertEquals(2, newsList.size());

    }

}