package at.oswald.ormapper;

import at.example.application.entity.Author;
import at.example.application.entity.Category;
import at.example.application.entity.News;
import at.oswald.helper.DDLExecutor;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

import static at.oswald.helper.HelperClass.getDBPropertiesForTesting;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class QueryTest {
    DBProperties testDBProperties = getDBPropertiesForTesting();
    EntityManager entityManager;
    Set<Class<?>> entityClasses = Set.of(News.class, Author.class, Category.class);
    News testEntity;

    @BeforeEach
    void setUpEach() {
        DDLExecutor ddlExecutor = new DDLExecutor(testDBProperties);
        ddlExecutor.dropDatabaseIfExists();
        ddlExecutor.createDatabaseIfNotExists();

        entityManager = EntityManager.getInstance(getDBPropertiesForTesting(), entityClasses);
        entityManager.initialize(entityClasses);
        testEntity = new News("title", "abc", true, Timestamp.valueOf(LocalDateTime.of(2020, 12, 12, 12, 5)), null, List.of());
        entityManager.getTransaction().begin();
        entityManager.persist(testEntity);

    }

    @AfterEach
    void tearDown() {
        entityManager.getTransaction().rollback();
        entityManager.close();
    }

    @Test
    void test_get_query_from_entityManager() {
        Query<?> nativeQuery = entityManager.createNativeQuery("SELECT * FROM news WHERE id = ?12", News.class);
        assertTrue(nativeQuery.getParameters().stream().anyMatch((parameter -> parameter.getPosition() == 12)));
        nativeQuery.setParameter(12, 1L);

        News entityFromDB = (News) nativeQuery.getResultList().get(0);
        assertEquals(testEntity, entityFromDB);

    }


    @Test
    void test_update_query_from_entityManager() {
        Query<?> nativeQuery = entityManager.createNativeQuery("UPDATE `news` SET `a_title` = :a_title, `text` = :text, `topnews` = :topnews, `timestamp` = :timestamp  WHERE `id` = :id", News.class);
        News news = new News(1L, "title", "abc", true, Timestamp.valueOf(LocalDateTime.of(2020, 12, 12, 12, 5)), null, List.of());

        assertTrue(nativeQuery.getParameters().stream().anyMatch((parameter -> parameter.getName().equals("a_title"))));
        assertTrue(nativeQuery.getParameters().stream().anyMatch((parameter -> parameter.getName().equals("text"))));

        nativeQuery.setParameter("id", news.getId());
        nativeQuery.setParameter("a_title", news.getTitle());
        nativeQuery.setParameter("text", news.getText());
        nativeQuery.setParameter("topnews", news.isTopNews());
        nativeQuery.setParameter("timestamp", news.getTimestamp());

        nativeQuery.executeUpdate();

        assertEquals(news, entityManager.find(News.class, 1L));

    }

    @Test
    void test_custom_update_query() {
        Query<?> nativeQuery = entityManager.createNativeQuery("UPDATE `news` SET `text` = :text WHERE `id` = :id", News.class);
        String newText = "this is a new text";

        assertTrue(nativeQuery.getParameters().stream().anyMatch((parameter -> parameter.getName().equals("id"))));
        assertTrue(nativeQuery.getParameters().stream().anyMatch((parameter -> parameter.getName().equals("text"))));

        nativeQuery.setParameter("id", 1L);
        nativeQuery.setParameter("text", newText);

        nativeQuery.executeUpdate();

        assertEquals(newText, entityManager.find(News.class, 1L).getText());
    }
}