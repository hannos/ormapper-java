package at.oswald.ormapper;

import at.example.application.entity.News;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class CacheTest {

    @Test
    void cache_entity_with_long_pk() {
        Cache cache = new Cache();
        News entity1 = new News(1L, "title", "abc", true, Timestamp.valueOf(LocalDateTime.of(2020, 12, 12, 12, 5)), null, null);

        cache.addEntity(entity1, 1L);

        News entityFromCache = cache.getEntity(entity1.getClass(), entity1.getId());
        assertNotNull(entityFromCache);

        assertEquals(entity1, entityFromCache);
    }

    @Test
    void test_remove_entity() {
        Cache cache = new Cache();
        News entity1 = new News(1L, "title", "abc", true, Timestamp.valueOf(LocalDateTime.of(2020, 12, 12, 12, 5)), null, null);

        cache.addEntity(entity1, 1L);

        assertTrue(cache.containsEntity(entity1.getClass(), entity1.getId()));

        cache.removeEntity(entity1, 1L);

        assertFalse(cache.containsEntity(entity1.getClass(), entity1.getId()));

    }

    @Test
    void test_remove_entity2() {
        Cache cache = new Cache();
        News entity1 = new News(1L, "title", "abc", true, Timestamp.valueOf(LocalDateTime.of(2020, 12, 12, 12, 5)), null, null);

        cache.addEntity(entity1, 1L);

        assertTrue(cache.containsEntity(entity1.getClass(), entity1.getId()));

        cache.removeEntity(entity1, entity1.getId());

        assertFalse(cache.containsEntity(entity1.getClass(), entity1.getId()));

    }


}