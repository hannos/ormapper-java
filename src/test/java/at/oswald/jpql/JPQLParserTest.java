package at.oswald.jpql;

import at.example.application.entity.Author;
import at.oswald.ormapper.ColumnInfo;
import at.oswald.ormapper.EntityInfo;
import at.oswald.ormapper.MappingBasic;
import at.oswald.ormapper.MappingInfo;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class JPQLParserTest {
    Map<Class<?>, EntityInfo> entitiesInfo = create_mock_EntitiesInfo();

    public static Map<Class<?>, EntityInfo> create_mock_EntitiesInfo() {
        String tableName = "t_author";
        List<MappingInfo> mappingInfoList = List.of(
                new MappingBasic("id", Long.class, new ColumnInfo(tableName, "id", "INT8", 0, true, true)),
                new MappingBasic("firstName", String.class, new ColumnInfo(tableName, "firstName", "VARCHAR")),
                new MappingBasic("lastName", Integer.class, new ColumnInfo(tableName, "lastName", "VARCHAR")),
                new MappingBasic("newsList", Double.class, new ColumnInfo(tableName, "newsList", "FLOAT"))
        );
        var entityInfo = EntityInfo.createFromEntityClass(Author.class, mappingInfoList);

        var entitiesInfo = new HashMap<Class<?>, EntityInfo>();
        entitiesInfo.put(Author.class, entityInfo);

        return entitiesInfo;
    }

    @Test
    void parse_select_missing_from() {
        String jpql = "select t TestEntity t where t.stringProperty = ?1";

        assertThrows(IllegalArgumentException.class, () -> new JPQLParser(jpql, entitiesInfo));
    }

    @Test
    void parse_update_not_supported() {
        String jpql = "update Author a where a.firstname = ?1";

        assertThrows(IllegalArgumentException.class, () -> new JPQLParser(jpql, entitiesInfo));
    }

    @Test
    void parse_valid() {
        String jpql = "select a from Author a where a.firstName = ?1";
        JPQLParser parser = new JPQLParser(jpql, entitiesInfo);
        String sql = parser.getNativeSQL();


        assertEquals("t_author", parser.getTableName());
        assertEquals(Author.class, parser.getReturnType());
        assertTrue(sql.equalsIgnoreCase("select * from `t_author` where firstName = ?1"));

    }

    @Test
    void parse_valid_with_like() {
        String jpql = "select a from Author a where a.firstName LIKE ?1 AND a.lastName = ?2";
        var parser = new JPQLParser(jpql, entitiesInfo);
        var sql = parser.getNativeSQL();


        assertEquals("t_author", parser.getTableName());
        assertNotNull(parser.getReturnType());
        assertEquals(Author.class, parser.getReturnType());
        assertTrue(sql.equalsIgnoreCase("select * from `t_author` where firstName like ?1 and lastName = ?2"));

    }

    @Test
    void parse_valid_with_and() {
        String jpql = "select a from Author a where a.firstName = ?1 AND a.lastName = ?2";
        JPQLParser parser = new JPQLParser(jpql, entitiesInfo);
        String sql = parser.getNativeSQL();


        assertEquals("t_author", parser.getTableName());
        assertNotNull(parser.getReturnType());
        assertEquals(Author.class, parser.getReturnType());
        assertTrue(sql.equalsIgnoreCase("select * from `t_author` where firstName = ?1 and lastName = ?2"));

    }

}