package at.oswald.newsexample;

import at.example.application.MainApp;
import at.example.application.entity.Author;
import at.example.application.entity.Category;
import at.example.application.entity.News;
import at.example.application.repository.AuthorRepository;
import at.example.application.repository.CategoryRepository;
import at.example.application.repository.NewsRepository;
import at.oswald.helper.DDLExecutor;
import at.oswald.ormapper.DBProperties;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.List;

import static at.oswald.helper.HelperClass.getDBPropertiesForTesting;
import static org.junit.jupiter.api.Assertions.*;


public class TestAuthorRepository {
    private static final DBProperties testDBProperties = getDBPropertiesForTesting();
    private static final DDLExecutor ddlExecutor = new DDLExecutor(testDBProperties);
    AuthorRepository authorRepository;
    NewsRepository newsRepository;
    CategoryRepository categoryRepository;

    @BeforeAll
    static void setUpAll() {
        ddlExecutor.dropDatabaseIfExists();
        ddlExecutor.createDatabaseIfNotExists();
    }

    @BeforeEach
    void setUpEach() {
        Injector injector = Guice.createInjector(MainApp.componentScanModule);
        authorRepository = injector.getInstance(AuthorRepository.class);
        newsRepository = injector.getInstance(NewsRepository.class);
        categoryRepository = injector.getInstance(CategoryRepository.class);
    }

    @AfterEach
    void afterEach() {
        newsRepository.deleteAll();
        authorRepository.deleteAll();
    }

    @Test
    void save_and_findAll_author() {
        Author author = new Author("Franz", "Kafka");
        authorRepository.save(author);
        List<Author> authors = authorRepository.findAll();

        assertEquals(1, authors.size());
        assertEquals(author, authors.get(0));

    }

    @Test
    void save_and_findAll_multiple_authors() {
        Author author1 = new Author("Franz", "Kafka");
        Author author2 = new Author("Thomas", "Mann");
        Author author3 = new Author("William", "Shakespeare");
        authorRepository.save(author1);
        authorRepository.save(author2);
        authorRepository.save(author3);

        List<Author> authors = authorRepository.findAll();

        assertEquals(3, authors.size());
        assertEquals(author1, authors.get(0));
        assertEquals(author2, authors.get(1));
        assertEquals(author3, authors.get(2));

    }

    @Test
    void saveAll_and_findAll_multiple_authors() {
        Author author1 = new Author("Franz", "Kafka");
        Author author2 = new Author("Thomas", "Mann");
        Author author3 = new Author("William", "Shakespeare");
        List<Author> authors = List.of(author1, author2, author3);
        authorRepository.saveAll(authors);

        List<Author> foundAuthors = authorRepository.findAll();

        assertEquals(3, foundAuthors.size());
        assertEquals(author1, foundAuthors.get(0));
        assertEquals(author2, foundAuthors.get(1));
        assertEquals(author3, foundAuthors.get(2));
    }

    @Test
    void saveAll_deleteAllInBatch_multiple_authors() {
        Author author1 = new Author("Franz", "Kafka");
        Author author2 = new Author("Thomas", "Mann");
        Author author3 = new Author("William", "Shakespeare");
        List<Author> authors = List.of(author1, author2, author3);
        authorRepository.saveAll(authors);

        var countBeforeDelete = authorRepository.findAll().size();
        authorRepository.deleteAll();
        var countAfterDelete = authorRepository.findAll().size();

        assertEquals(3, countBeforeDelete);
        assertEquals(0, countAfterDelete);

    }

    @Test
    void saveAll_deleteAll_multiple_authors() {
        Author author1 = new Author("Franz", "Kafka");
        Author author2 = new Author("Thomas", "Mann");
        Author author3 = new Author("William", "Shakespeare");
        List<Author> authors = List.of(author1, author2, author3);
        authorRepository.saveAll(authors);

        int countBeforeDelete = authorRepository.findAll().size();
        authorRepository.deleteAll(List.of(author1, author2));
        int countAfterDelete = authorRepository.findAll().size();

        assertEquals(3, countBeforeDelete);
        assertEquals(1, countAfterDelete);

    }

    @Test
    void find_Author_by_Firstname() {
        Author author1 = new Author("Thomas", "Glavinic");
        Author author2 = new Author("Thomas", "Mann");
        Author author3 = new Author("William", "Shakespeare");
        authorRepository.save(author1);
        authorRepository.save(author2);
        authorRepository.save(author3);

        List<Author> authors = authorRepository.findAuthorByFirstname("Thomas");

        assertEquals(2, authors.size());
        assertEquals(author1, authors.get(0));
        assertEquals(author2, authors.get(1));

    }

    @Test
    void save_author_with_null_news() {
        Author author = new Author("Thomas", "Glavinic");
        authorRepository.save(author);

        assertEquals(1, authorRepository.findAll().size());
        assertNotNull(author.getId());

        News news = new News("Neuer Weltrekord!", "Der Deutsche Manfred H. ist der neue..", false, Timestamp.from(Instant.now()), null);
        newsRepository.save(news);

        List<News> allNews = newsRepository.findAll();
        assertEquals(1, allNews.size());
        assertNull(allNews.get(0).getAuthor());
    }

    @Test
    void save_author_with_news() {
        Author author = new Author("Thomas", "Glavinic");
        authorRepository.save(author);

        assertEquals(1, authorRepository.findAll().size());
        assertNotNull(author.getId());


        News news = new News("Neuer Weltrekord!", "Der Deutsche Manfred H. ist der neue..", false, Timestamp.from(Instant.now()), author);
        newsRepository.save(news);

        List<News> allNews = newsRepository.findAll();
        assertEquals(1, allNews.size());
        news = allNews.get(0);
        assertNotNull(news);
        assertEquals(authorRepository.findAll().get(0), news.getAuthor());

    }

    @Test
    void save_author_with_news_findByTitle() {
        Author author = new Author("Thomas", "Glavinic");
        authorRepository.save(author);

        News news1 = new News("Neuer Weltrekord!", "Der Deutsche Manfred H. ist der neue..", false, Timestamp.from(Instant.now()), author);
        News news2 = new News("Neues Hochhaus", "Neues Hochhaus errichtet", true, Timestamp.from(Instant.now()), author);
        News news3 = new News("Koala Nachwuchs", "Koala Karl geboren", false, Timestamp.from(Instant.now()), author);
        newsRepository.saveAll(List.of(news1, news2, news3));

        List<News> allNews = newsRepository.findAll();
        assertEquals(3, allNews.size());

        List<News> neuNews = newsRepository.findNewsWithTitleLike("%Neu%");
        assertEquals(2, neuNews.size());

        author = neuNews.get(0).getAuthor();

        assertNotNull(author);


        allNews = newsRepository.findAll();
        assertEquals(3, allNews.size());
        assertNotNull(author.getNewsList());
        assertEquals(3, author.getNewsList().size());
        assertEquals(news1.getTitle(), author.getNewsList().get(0).getTitle());

    }

    @Test
    void test_delete_author_from_existing_news() {
        Author author = new Author("Thomas", "Glavinic");
        authorRepository.save(author);

        News news = new News("Neuer Weltrekord!", "Der Deutsche Manfred H. ist der neue..", false, Timestamp.from(Instant.now()), author);
        newsRepository.save(news);

        assertEquals(1, authorRepository.findAll().size());
        assertEquals(1, newsRepository.findAll().size());

        assertNotNull(newsRepository.findAll().get(0).getAuthor());

        authorRepository.delete(author);
        assertEquals(0, authorRepository.findAll().size());

    }

    @Test
    void test_toString() {
        Author author = new Author("Thomas", "Glavinic");
        authorRepository.save(author);
        News news = new News("Neuer Weltrekord!", "Der Deutsche Manfred H. ist der neue..", false, Timestamp.valueOf(LocalDateTime.of(2020, 12, 1, 10, 0)), author);
        newsRepository.save(news);

        Category category = new Category("Sport", List.of(news));
        categoryRepository.save(category);

        Author authorFromFind = authorRepository.findById(author.getId());
        assertEquals(author, authorFromFind);

        News newsFromFind = newsRepository.findById(news.getId());
        assertEquals(news, newsFromFind);
        assertEquals(author.getNewsList(), authorFromFind.getNewsList());
        assertEquals(author.toString(), authorFromFind.toString());

    }


}
