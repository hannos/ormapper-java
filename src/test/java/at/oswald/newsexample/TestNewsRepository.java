package at.oswald.newsexample;

import at.example.application.MainApp;
import at.example.application.entity.Category;
import at.example.application.entity.News;
import at.example.application.repository.CategoryRepository;
import at.example.application.repository.NewsRepository;
import at.oswald.helper.DDLExecutor;
import at.oswald.ormapper.DBProperties;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

import static at.oswald.helper.HelperClass.getDBPropertiesForTesting;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class TestNewsRepository {
    private static final DBProperties testDBProperties = getDBPropertiesForTesting();
    private static final DDLExecutor ddlExecutor = new DDLExecutor(testDBProperties);
    CategoryRepository categoryRepository;
    NewsRepository newsRepository;


    @BeforeAll
    static void setUpAll() {
        ddlExecutor.dropDatabaseIfExists();
        ddlExecutor.createDatabaseIfNotExists();
    }

    @BeforeEach
    void setUpEach() {
        Injector injector = Guice.createInjector(MainApp.componentScanModule);
        categoryRepository = injector.getInstance(CategoryRepository.class);
        newsRepository = injector.getInstance(NewsRepository.class);
    }

    @AfterEach
    void afterEach() {
        categoryRepository.deleteAll();
        newsRepository.deleteAll();
    }

    @Test
    void test_news_get_categories_many_to_many() {
        Category category1 = new Category("Sport");
        Category category2 = new Category("Regionales");
        categoryRepository.saveAll(List.of(category1, category2));
        assertNotNull(category1.getId());
        News news1 = new News("Bergrallye", "In Hintertux", true, Timestamp.from(Instant.now()), null, List.of(category1, category2));
        newsRepository.save(news1);

        assertNotNull(news1.getId());

        News newsFromRepo = newsRepository.findById(news1.getId());
        assertNotNull(newsFromRepo);
        assertNotNull(newsFromRepo.getCategories());
        assertEquals(2, newsFromRepo.getCategories().size());
        assertNotNull(newsFromRepo.getCategories().get(0));
        assertNotNull(newsFromRepo.getCategories().get(0).getId());
        assertNotNull(newsFromRepo.getCategories().get(1));
        assertNotNull(newsFromRepo.getCategories().get(1).getId());
    }

    @Test
    void test_news_remove() {
        Category category1 = new Category("Sport");
        Category category2 = new Category("Regionales");
        categoryRepository.saveAll(List.of(category1, category2));
        assertNotNull(category1.getId());
        News news1 = new News("Bergrallye", "In Hintertux", true, Timestamp.from(Instant.now()), null, List.of(category1, category2));
        newsRepository.save(news1);

        assertNotNull(news1.getId());

        List<News> allNews = newsRepository.findAll();

        assertEquals(1, allNews.size());
    }

    @Test
    void test_find_get_m_to_m_attributes() {
        Category category1 = new Category("Sport");
        Category category2 = new Category("Regionales");
        categoryRepository.saveAll(List.of(category1, category2));
        assertNotNull(category1.getId());
        News news1 = new News("Bergrallye", "In Hintertux", true, Timestamp.from(Instant.now()), null, List.of(category1, category2));
        newsRepository.save(news1);
        News news2 = new News("Title", "Text", true, Timestamp.from(Instant.now()), null, List.of(category1, category2));
        newsRepository.save(news2);


        List<News> allNews = newsRepository.findAll();

        assertEquals(2, allNews.size());
        for (var news : allNews) {
            assertEquals(2, news.getCategories().size());
        }

        List<Category> categoryList = categoryRepository.findAll();

        for (var category : categoryList) {
            assertEquals(2, category.getNewsList().size());
        }

    }
}
