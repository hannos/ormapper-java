package at.oswald.sql;

import at.example.application.entity.Author;
import at.example.application.entity.Category;
import at.example.application.entity.News;
import at.oswald.ormapper.*;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.*;

class MappingFactoryTest {

    @Test
    void test_createMapping_Basic() throws NoSuchFieldException {
        String basicFieldName = "firstName";
        Field field = Author.class.getDeclaredField(basicFieldName);
        MappingInfo mappingInfo = MappingFactory.createMapping(field);

        assertTrue(mappingInfo instanceof MappingBasic);
        assertNull(mappingInfo.getRelatedEntityClass());
        assertEquals(basicFieldName.toLowerCase(), mappingInfo.getColumnInfoEntity().getColumnName());
        assertFalse(mappingInfo.getColumnInfoEntity().isPrimaryKey());
    }

    @Test
    void test_createMapping_Basic_PK() throws NoSuchFieldException {
        String idFieldName = "id";
        Field field = Author.class.getDeclaredField(idFieldName);
        MappingInfo mappingInfo = MappingFactory.createMapping(field);

        assertTrue(mappingInfo instanceof MappingBasic);
        assertNull(mappingInfo.getRelatedEntityClass());
        assertEquals(idFieldName.toLowerCase(), mappingInfo.getColumnInfoEntity().getColumnName());
        assertTrue(mappingInfo.getColumnInfoEntity().isPrimaryKey());
    }

    @Test
    void test_createMapping_One_To_Many() throws NoSuchFieldException {
        String oneToManyFieldName = "newsList";
        Field field = Author.class.getDeclaredField(oneToManyFieldName);
        MappingInfo mappingInfo = MappingFactory.createMapping(field);

        assertTrue(mappingInfo instanceof MappingOneToMany);
        assertEquals(News.class, mappingInfo.getRelatedEntityClass());
        assertNull(mappingInfo.getColumnInfoEntity());
        assertEquals("author_id", mappingInfo.getColumnInfoRelatedEntity().getColumnName());
        assertEquals("newsList", mappingInfo.getFieldName());

    }

    @Test
    void test_createMapping_Many_To_One() throws NoSuchFieldException {
        String manyToOneFieldName = "author";
        Field field = News.class.getDeclaredField(manyToOneFieldName);
        MappingInfo mappingInfo = MappingFactory.createMapping(field);

        assertTrue(mappingInfo instanceof MappingManyToOne);
        assertEquals(Author.class, mappingInfo.getRelatedEntityClass());
        assertEquals("author_id", mappingInfo.getColumnInfoEntity().getColumnName());
    }

    @Test
    void test_createMapping_Many_To_Many() throws NoSuchFieldException {
        String manyToManyFieldName = "categories";
        Field field = News.class.getDeclaredField(manyToManyFieldName);
        MappingInfo mappingInfo = MappingFactory.createMapping(field);

        assertTrue(mappingInfo instanceof MappingManyToMany);
        assertEquals(Category.class, mappingInfo.getRelatedEntityClass());
        assertEquals("news_id", mappingInfo.getColumnInfoEntity().getColumnName());
        assertEquals("category_id", mappingInfo.getColumnInfoRelatedEntity().getColumnName());
        assertEquals("categories", mappingInfo.getFieldName());


    }

}