package at.oswald.sql;

import at.example.application.entity.News;
import at.oswald.ormapper.ColumnInfo;
import at.oswald.ormapper.EntityInfo;
import at.oswald.ormapper.MappingBasic;
import at.oswald.ormapper.MappingInfo;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

class InsertStatementTest {


    public static EntityInfo create_mock_EntityInfo() {
        String tableName = "news";
        List<MappingInfo> mappingInfoList = List.of(
                new MappingBasic("id", Long.class, new ColumnInfo(tableName, "id", "INT8", null, true, true)),
                new MappingBasic("title", String.class, new ColumnInfo(tableName, "a_title", "VARCHAR", null, false, false)),
                new MappingBasic("text", String.class, new ColumnInfo(tableName, "text", "VARCHAR", null, false, false)),
                new MappingBasic("topnews", Boolean.class, new ColumnInfo(tableName, "topnews", "BOOLEAN", null, false, false)),
                new MappingBasic("timestamp", Timestamp.class, new ColumnInfo(tableName, "timestamp", "TIMESTAMP", null, false, false))
        );


        return EntityInfo.createFromEntityClass(News.class, mappingInfoList);
    }

    @Test
    void getSQL() {
        var entityInfo = create_mock_EntityInfo();
        var insert = new InsertStatement(entityInfo.getTableName(), entityInfo.getColumnInfos());
        String sql = insert.getSQL();


        assertTrue(sql.contains("INSERT INTO `news`"));
        assertTrue(sql.contains("`a_title`"));
        assertTrue(sql.contains("`text`"));
        assertTrue(sql.contains("`topnews`"));
        assertTrue(sql.contains("`timestamp`"));
        assertTrue(sql.contains("VALUES (  :id, :a_title, :text, :topnews, :timestamp)"));
    }

}