package at.oswald.sql;

import at.example.application.entity.Author;
import at.example.application.entity.Category;
import at.example.application.entity.News;
import at.oswald.ormapper.ColumnInfo;
import at.oswald.ormapper.EntityInfo;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class EntityInfoTest {


    @Test
    void valid_entity_should_return_matching_columns() {
        EntityInfo entityInfo = EntityInfo.createFromEntityClass(News.class);
        var columns = entityInfo.getColumnInfos();

        assertTrue(columns.stream().anyMatch((c) -> equalsColumnInfo(c, "id", "INT8", true)));
        assertTrue(columns.stream().anyMatch((c) -> equalsColumnInfo(c, "a_title", "VARCHAR", false)));
        assertTrue(columns.stream().anyMatch((c) -> equalsColumnInfo(c, "text", "VARCHAR", false)));
        assertTrue(columns.stream().anyMatch((c) -> equalsColumnInfo(c, "topnews", "BOOLEAN", false)));
        assertTrue(columns.stream().anyMatch((c) -> equalsColumnInfo(c, "timestamp", "TIMESTAMP", false)));
        assertTrue(columns.stream().anyMatch((c) -> equalsColumnInfo(c, "author_id", "INT8", false)));

    }

    @Test
    void createFromEntity() {
        EntityInfo entityInfo = EntityInfo.createFromEntityClass(News.class);
        assertEquals("news", entityInfo.getTableName());
    }

    @Test
    void test_get_related_entities_author() {
        EntityInfo entityInfo = EntityInfo.createFromEntityClass(Author.class);
        Set<Class<?>> relatedEntities = entityInfo.getRelatedEntities();

        assertTrue(relatedEntities.contains(News.class));

    }

    @Test
    void test_get_related_entities_category() {
        EntityInfo entityInfo = EntityInfo.createFromEntityClass(Category.class);
        Set<Class<?>> relatedEntities = entityInfo.getRelatedEntities();

        assertTrue(relatedEntities.contains(News.class));

    }

    @Test
    void test_get_related_entities_news() {
        EntityInfo entityInfo = EntityInfo.createFromEntityClass(News.class);
        Set<Class<?>> relatedEntities = entityInfo.getRelatedEntities();

        assertTrue(relatedEntities.containsAll(List.of(Author.class, Category.class)));

    }

    boolean equalsColumnInfo(ColumnInfo column, String expectedColumnName, String expectedDataType, boolean expectedIsPrimaryKey) {
        return expectedColumnName.equals(column.getColumnName()) && (expectedDataType.equals(column.getDataType())) && (expectedIsPrimaryKey == column.isPrimaryKey());
    }

}