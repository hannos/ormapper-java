package at.oswald.sql;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class SelectStatementTest {

    @Test
    void select_statement_with_one_where_clause() {
        var selectStatement = SelectStatement.forTable("testentity")
                .addEqualsToWhereClause("entityId", 1);
        String sql = selectStatement.getSQL();

        assertTrue(sql.contains(" WHERE entityId = ?"));
    }

    @Test
    void select_statement_with_multiple_where_clause() {
        SelectStatement selectStatement = SelectStatement.forTable("testentity")
                .addToWhereClause("stringProperty = ?")
                .addToWhereClause(" AND intProperty = ?");
        String sql = selectStatement.getSQL();

        assertTrue(sql.contains(" WHERE stringProperty = ?"));
        assertTrue(sql.contains(" AND intProperty = ?"));

    }
}