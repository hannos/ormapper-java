package at.oswald.sql;

import at.oswald.ormapper.ColumnInfo;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static at.oswald.sql.InsertStatementTest.create_mock_EntityInfo;
import static org.junit.jupiter.api.Assertions.*;

class UpdateStatementTest {

    @Test
    void test_update() {
        var entityInfo = create_mock_EntityInfo();

        var stmt = new UpdateStatement(entityInfo.getTableName(),
                entityInfo.getPrimaryKeyColumn(),
                entityInfo.getNonPrimaryKeyColumns());
        assertEquals("UPDATE `news` SET  `topnews` = :topnews, `a_title` = :a_title, `text` = :text, `timestamp` = :timestamp WHERE `id` = :id", stmt.getSQL());
    }

    @Test
    void test_update_with_one_field() {
        var entityInfo = create_mock_EntityInfo();
        ColumnInfo columnInfo = entityInfo.getMappingForField("title").getColumnInfoEntity();
        ColumnInfo primaryKeyColumn = entityInfo.getPrimaryKeyColumn();

        assertNotNull(columnInfo);

        var stmt = new UpdateStatement(entityInfo.getTableName(), primaryKeyColumn, columnInfo);
        assertEquals("UPDATE `news` SET  `a_title` = :a_title WHERE `id` = :id", stmt.getSQL());
    }

    @Test
    void test_update_with_three_fields() {
        var entityInfo = create_mock_EntityInfo();
        ColumnInfo columnInfo1 = entityInfo.getMappingForField("title").getColumnInfoEntity();
        ColumnInfo columnInfo2 = entityInfo.getMappingForField("text").getColumnInfoEntity();
        ColumnInfo columnInfo3 = entityInfo.getMappingForField("topnews").getColumnInfoEntity();
        ColumnInfo primaryKeyColumn = entityInfo.getPrimaryKeyColumn();

        assertNotNull(columnInfo1);

        var stmt = new UpdateStatement(entityInfo.getTableName(), primaryKeyColumn, List.of(columnInfo1, columnInfo2, columnInfo3));
        assertEquals("UPDATE `news` SET  `topnews` = :topnews, `a_title` = :a_title, `text` = :text WHERE `id` = :id", stmt.getSQL());
    }

    @Test
    void test_with_parameterColumnInfoMap() {
        var entityInfo = create_mock_EntityInfo();
        ColumnInfo columnInfoForPar1 = entityInfo.getMappingForField("title").getColumnInfoEntity();
        ColumnInfo columnInfoForPar2 = entityInfo.getMappingForField("text").getColumnInfoEntity();
        ColumnInfo primaryKeyColumn = entityInfo.getPrimaryKeyColumn();


        var stmt = new UpdateStatement(entityInfo.getTableName(), primaryKeyColumn, Map.of("par1", columnInfoForPar1, "par2", columnInfoForPar2));
        assertTrue(stmt.getSQL().contains("UPDATE `news` SET"));
        assertTrue(stmt.getSQL().contains("`text` = :par2"));
        assertTrue(stmt.getSQL().contains("`a_title` = :par1"));
        assertTrue(stmt.getSQL().contains("WHERE `id` = :id"));

    }

}