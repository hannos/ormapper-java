package at.oswald.sql;


import at.oswald.ormapper.ColumnInfo;
import org.junit.jupiter.api.Test;

import java.sql.Time;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DDLStatementTest {

    @Test
    void buildCreateTableIfNotExitsStatement() {
        var tableName = "testTable";
        ColumnInfo column1 = new ColumnInfo(tableName, "entityId", "INT", null, true, true);
        ColumnInfo column2 = new ColumnInfo(tableName, "myStryng", "VARCHAR", 100, false, false);
        ColumnInfo column3 = new ColumnInfo(tableName, "intProperty", "INT", null, false, false);

        String sql = DDLStatement.buildCreateTableIfNotExitsStatement(tableName, List.of(column1, column2, column3));
        assertTrue(sql.contains("CREATE TABLE IF NOT EXISTS `" + tableName + "`"));
        assertTrue(sql.contains("PRIMARY KEY (`entityid`)"));
        assertTrue(sql.contains("`mystryng` VARCHAR(100) ,"));
        assertTrue(sql.contains("`intproperty` INT ,"));
    }

    @Test
    void buildSelectStatement() {
        var tableName = "myDB";

        var SQLStatement = SelectStatement.forTable(tableName).getSQL();

        assertTrue(SQLStatement.contains(String.format("SELECT * FROM `%s`", tableName)));

    }

    @Test
    void getSQLTypeForClass_should_return_int_for_a_integer_class() {
        String sqlType = DDLStatement.getSQLTypeForClass(Integer.class);
        assertEquals("INT", sqlType);
    }

    @Test
    void getSQLTypeForClass_should_return_varchar_for_a_string_class() {
        String sqlType = DDLStatement.getSQLTypeForClass(String.class);
        assertEquals("VARCHAR", sqlType);
    }

    @Test
    void getSQLTypeForClass_should_throw_exception_for_a_undefined_class() {
        assertThrows(IllegalArgumentException.class, () -> DDLStatement.getSQLTypeForClass(Time.class));
    }
}