# ORMapper for Java

* Author: Hannes Oswald (if18b040)
* Contact for questions: if18b040@technikum-wien.at
* Git Repo: https://gitlab.com/hannos/ormapper-java

## Introduction

This OR mapper is based on the javax.persistence API (JPA). (
see https://docs.oracle.com/javaee/7/api/javax/persistence/package-summary.html) It is supposed to work similar to
implementations of this API e.g. Hibernate. Of course not all features of this API are implemented.

As an extra feature, data repositories such as in Spring Data can be created via Dependency Injection. (
see: https://docs.spring.io/spring-data/data-commons/docs/1.6.1.RELEASE/reference/html/repositories.html) Again not all
features such as in Spring Data are supported.

## Features

* Persistence of entities
* Persistence of changes
* Caching
* Loading of entities from the DB
* JPQL for arbitrary queries (see: https://docs.oracle.com/javaee/6/tutorial/doc/bnbtg.html)
* 1:n relations
* m:n relations
* Transactions
* Data Repositories similar to Spring Data Repositories

## Requirements

* Java 11
* MariaDB 10.4 (MYSQL should work too, but was not tested)
* Maven

## Set-Up

* Requires a running MariaDB database
* E.g. Docker: <code>docker run --name some-mariadb -e MYSQL_ROOT_PASSWORD=pwd -e MYSQL_DATABASE=testdb -p 3306:3306 -d
  mariadb:10.4</code>
* Schema/Database must be created before starting the application
* Set database configuration in src/main/resources/database.yml

## Test Application

The test application is a small management software for news articles. It uses the implemented Data Repositories to save
the data to the database. The Data Repositories use the EntityManager to interact with the database. The EntityManger
can also be used directly, for examples see: <code>src/test/java/at/oswald/ormapper/EntityManagerTest.java</code>

## Packages

* <code>at.example.application.*</code> contains the example app
* <code>at.oswald.*</code> contains the code for the ORM

## Usage

### Entity Classes

#### Requirements

* The ID property must be annotated with <code>@Id</code>
* The ID property must be <code>Integer</code> or <code>Long</code>
* The other properties must be annotated with <code>@Column</code>

#### Example

```java

@Entity
@NoArgsConstructor
public class Person {
    @Id
    private Long personId;

    @Column(nullable = false, length = 50)
    public String name;

    @Column
    private int age;

    @ManyToOne
    private Home home;

    @ManyToMany
    private List<Thing> things;
}
```

### ORMapper Interface

There are two ways to work with the ORMapper:

1. Use the EntityManager -> see <code>src/test/java/at/oswald/ormapper/EntityManagerTest.java</code>
2. Data Repositories -> see Example Application

### Repository

#### Requirements

* The repository for the entity must extend <code>BaseJpaRepository<T, ID></code>
* T is the Entity and ID is the type of the ID property
* Basic Methods like <code>save</code>, <code>findAll</code> are included in the BaseJpaRepository
* Custom Queries can be defined in the repository interface with the annotation <code>@JPQLQuery</code> and the JPQL
  query as value

#### Example

```java
public interface PersonRepository extends BaseJpaRepository<Person, Integer> {
    @JPQLQuery("select p from Person p where p.name = ?1")
    List<Person> findAllByName(String name);
}
```

## (JPQL) Queries

JPQL Queries can also be used with the EntityManger.

```java
public class Example() {

    public void method() {
        Query<?> query = entityManager.createQuery("select p from Person p where p.name = ?1", Person.class);

        query.setParameter(1, "Hans Dampf");

        List<Person> persons = query.getResultList();
    }

}
```

#### Requirements

* Only select statements are supported.
* Select statement can only return whole objects.
* Supported expressions in WHERE clause:
  * Literals: String, Numeric, Boolean
  * Navigation Operator: .
  * Comparison Operator: =
  * Logical Operators: NOT, AND, OR
  * String pattern: LIKE